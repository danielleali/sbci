<?php 
/**
 * Template Name: Why SBCI?
 * Template Post Type: page
 * 
 * Template Description...
 **/
if ( have_posts() ) :
	while ( have_posts() ) : 
		the_post(); 
		get_template_part('templates/why-sbci');
	endwhile; 
endif; 
?>