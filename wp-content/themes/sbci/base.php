<?php get_template_part('templates/head'); ?>
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
<script src="/wp-content/themes/assets/js/plugins/masonry.pkgd.min.js"></script>
<body <?php body_class(); ?>>

  
    <?php
    do_action('get_header');
    get_template_part('templates/header');
    ?>
    
    <!-- <div class="wrapper">
      <main class="container"> -->
        <?php include ct_template_path(); ?>
      <!-- </main>
    </div> -->

    <?php get_template_part('templates/footer'); ?>
  

</body>
</html>