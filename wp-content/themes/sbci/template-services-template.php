<?php 
/**
 * Template Name: Services Template
 * Template Post Type: page
 * 
 * Template Description...
 **/
if ( have_posts() ) :
	while ( have_posts() ) : 
		the_post(); 
		get_template_part('templates/services-template');
	endwhile; 
endif; 
?>