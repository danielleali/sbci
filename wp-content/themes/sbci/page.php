<?php 
if ( have_posts() ) :
	while ( have_posts() ) : 
		the_post(); 
		get_template_part('templates/content', $post->post_name);
	endwhile; 
endif; 
?>