/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 *
 * Google CDN, Latest jQuery
 * To use the default WordPress version of jQuery, go to lib/config.php and
 * remove or comment out: add_theme_support('jquery-cdn');
 * ======================================================================== */

(function($) {

    $.resizeend = function(el, options){
      var base = this;

      base.$el = $(el);
      base.el = el;

      base.$el.data("resizeend", base);
      base.rtime = new Date(1, 1, 2000, 12,00,00);
      base.timeout = false;
      base.delta = 200;

      base.init = function(){
          base.options = $.extend({},$.resizeend.defaultOptions, options);

          if(base.options.runOnStart) base.options.onDragEnd();

          $(base.el).resize(function() {

              base.rtime = new Date();
              if (base.timeout === false) {
                  base.timeout = true;
                  setTimeout(base.resizeend, base.delta);
              }
          });

      };
      base.resizeend = function() {
          if (new Date() - base.rtime < base.delta) {
              setTimeout(base.resizeend, base.delta);
          } else {
              base.timeout = false;
              base.options.onDragEnd();
          }
      };

      base.init();
  };

  $.resizeend.defaultOptions = {
      onDragEnd : function() {},
      runOnStart : false
  };

  $.fn.resizeend = function(options){
      return this.each(function(){
          (new $.resizeend(this, options));
      });
  };

  jQuery.fn.scrollOffset = function () {
      var win = $(window);
      var topSpace = this.offset().top - win.scrollTop();
      var bottomSpace = win.height() - this.height() - topSpace;
      var leftSpace = this.offset().left;
      var rightSpace = win.width() - this.width() - leftSpace;
      return { top: topSpace, bottom: bottomSpace, left: leftSpace, right: rightSpace };
  };

  function isElementInViewport (el, verticaloffset) {
    var win = $(window);

    var viewport = {
        top : win.scrollTop(),
        left : win.scrollLeft()
    };
    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height();

    var bounds = el.offset();
    bounds.right = bounds.left + el.outerWidth();
    bounds.bottom = bounds.top + el.outerHeight();

    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

  }

  function viewport() {
    var e = window, a = 'inner';
    if (!('innerWidth' in window )) {
        a = 'client';
        e = document.documentElement || document.body;
    }
    return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
  }





// Use this variable to set up the common and page specific functions. If you
// rename this variable, you will also need to rename the namespace below.
var Roots = {

  /*
    Preload
  */
  preload: {
    init: function() {

      function set_animation(el) {
        el.addClass('animated');
        if (el.hasClass('down')) {
          el.addClass('fadeInDown');
          el.removeClass('down');
        } else if (el.hasClass('left')) {
          el.addClass('fadeInLeft');
          el.removeClass('left');
        } else if (el.hasClass('right')) {
          el.addClass('fadeInRight');
          el.removeClass('right');
        } else if (el.hasClass('in')) {
          el.addClass('fadeIn');
          el.removeClass('in');
        } else if (el.hasClass('up')) {
          el.addClass('fadeInUp');
          el.removeClass('up');
        }
      }

      /* Trigger Scrolling Animation */
      $(window).on('scroll', function () {
        $('.scrolling').each(function() {
          if ( isElementInViewport($(this), 100) ) {
            $(this).removeClass('scrolling');
            set_animation($(this));
          }
        }); 
      });
      setTimeout(function(){
        $('.scrolling').each(function() {
          if ( isElementInViewport($(this), 100) ) {
            $(this).removeClass('scrolling');
            set_animation($(this));
          }
        });
      },500);

      $(window).on("load",function() {
        $(window).scroll(function() {
          var windowBottom = $(this).scrollTop() + $(this).innerHeight();
          $(".fade").each(function() {
            /* Check the location of each desired element */
            var objectBottom = $(this).offset().top + $(this).outerHeight();
            
            /* If the element is completely within bounds of the window, fade it in */
            if (objectBottom < windowBottom) { //object comes into view (scrolling down)
              if ($(this).css("opacity")==0) {$(this).fadeTo(500,1);}
            } else { //object goes out of view (scrolling up)
              if ($(this).css("opacity")==1) {$(this).fadeTo(500,0);}
            }
          });
        }).scroll(); //invoke scroll-handler on page-load
      });

      // Featured Content number toggles - click to make active
      $('.pagination-container ol .item').on('click', function () {
        $('.item').removeClass('active');
        $(this).addClass('active');
      });


      // Navigation Menu - Show Membership Dropdowns whe user clicks on Membership button
      $('.nav-top__membership').on('click', function () {
        $('.membership-dropdowns').slideToggle();
        $('.nav-top__membership a').toggleClass('clicked');
      });

      $(".nav-top__membership").mouseenter(function(){
        $('.membership-dropdowns').slideDown();
      });
    
      $(".membership-dropdowns").mouseleave(function(){
        $('.membership-dropdowns').slideUp();
      });


      // Navigation Menu - Show Membership Login/Registration whe user clicks on Employee Portal button
      $('.nav-top__employee').on('click', function () {

        $('.modal__container').addClass('modal__open');

        setTimeout(function(){
          $('.modal__container .modal').removeClass('hide');
          $('.modal__container .modal').addClass('transition');
        });

        setTimeout(function(){
          $('.modal__container .modal').removeClass('transition');
          $('.modal__container .modal').addClass('visible');
        },50);

        $('.modal__close').on('click', function(){
          $('.modal__container').removeClass('modal__open');
          $('.modal').removeClass('visible');
          $('.modal').addClass('transition');
          setTimeout(function(){
            $('.modal').removeClass('transition');
            $('.modal').addClass('hide');
          },500);
        });
          

        $(document).keydown(function(e){
          if(e.keyCode == 27) {
            $('.modal__container').removeClass('modal__open');
            $('.modal').removeClass('visible');
            $('.modal').addClass('transition');
            setTimeout(function(){
              $('.modal').removeClass('transition');
              $('.modal').addClass('hide');
            },500);
          }
        });

      });

      


      // open secondary menu on click - mobile
      $('.nav-mobile .nav-bottom .nav-main ul li' ).on('click', function (event) { 
        $('.nav-mobile .nav-bottom .nav-main ul li').not(this).removeClass('open');
        $(this).toggleClass('open');
        event.preventDefault();
      });

      // open tertiary menu on click
      $('.dropdown-menu__primary li' ).on('click', function (event) {
        $('.dropdown-menu__primary li').not(this).removeClass('open-second');
        $(this).toggleClass('open-second');
        event.preventDefault();
      });

      // open search bar on click, but only if screen is at 992px or wider
      $('.nav-desktop .nav-right .nav-top #search-icon').on('click', function () {

        $('.search_expanded').animate({width:'toggle'},500);
        $(".nav-desktop .nav-right .nav-top").toggleClass("search-clicked");
        
        if ($(window).width() < 992) {
          // less than 992, do nothing
        }
        else {

        }
      });

      // Toggle Search bar on Escape key press
      $(document).on('keydown', function(event) {
        if (event.key == "Escape") {
            $('.search_expanded').animate({width:'toggle'},500);
            $(".nav-desktop .nav-right .nav-top").toggleClass("search-clicked");
        }
      });

      // open search bar on click, but only if screen is at 992px or wider
      $('.nav-mobile .nav-middle #search-icon').on('click', function () {
        $(".nav-mobile .nav-middle .brand").toggle();
        $('.search_expanded').animate({width:'toggle'},500);
        $(".nav-mobile .nav-middle").toggleClass("search-clicked");
      });


      // .nav-mobile .nav-middle 
      // dropdown-menu dropdown-menu__primary
      // dropdown-menu dropdown-menu__secondary
      // Dropdown menu (secondary menu) on hover
      $( ".section__header .nav-desktop .nav-bottom .nav-main ul li ul" ).mouseover(function() {
        // $(this).css("right", "0");
      });


      /* Smooth Scroll */
      $('.scroll-top').click(function(){
        $('html, body').animate({
          scrollTop: $( $(this).attr('href') ).offset().top
        }, 700);
        return false;
      });





      // Pull Quote toggles - click to make active
      $('.pull_quote_new .toggles ul li').on('click', function () {
        $(this).parent().find('li').removeClass('active');
        // $('.pull_quote .quote_bottom .toggles ul li').removeClass('active');
        $(this).addClass('active');
      });





      /* Pull Quote Slider */
      var currentIndex_testimonial_slider = 0,
      items_testimonial_slider = $('.pull_quote_new_slider > ul li'),
      toggle_testimonial_slider = $('.pq_slider__toggle.autoslide li'),
      itemAmt_testimonial_slider = items_testimonial_slider.length;

      $quote_height_full = $('.pull_quote_new.full .pull_quote_new_slider ul li ').outerHeight();
      $('.pull_quote_new.full .pull_quote_new_slider').css("height", $quote_height_full);

      $quote_height_half = $('.pull_quote_new.half .pull_quote_new_slider ul li ').outerHeight();
      $('.pull_quote_new.half .pull_quote_new_slider').css("height", $quote_height_half);

      $(window).resize(function(){
        $quote_height_full = $('.pull_quote_new.full .pull_quote_new_slider ul li ').outerHeight();
        $('.pull_quote_new.full .pull_quote_new_slider').css("height", $quote_height_full);
  
        $quote_height_half = $('.pull_quote_new.half .pull_quote_new_slider ul li ').outerHeight();
        $('.pull_quote_new.half .pull_quote_new_slider').css("height", $quote_height_half);
      });

      /* Toggles */
      $('.pq_slider__toggle li').on('click', function(event) {
        event.preventDefault();
        var slide = $(this).attr('data-slide');
        var slide_num = parseInt(slide);
        var leftPos = $(this).closest('.page_section').find('.pull_quote_new_slider > ul').scrollLeft(),
          itemSize = $(this).closest('.page_section').find('.pull_quote_new_slider li').outerWidth();
          ulSize = $(this).closest('.page_section').find('.pull_quote_new_slider > ul').outerWidth();
          ulSize = parseInt(ulSize);
        
        $quote_height = $(this).closest('.pull_quote_new').find('.pull_quote_new_slider ul li').outerHeight();
        $(this).closest('.pull_quote_new').find('.pull_quote_new_slider').css("height", $quote_height);

        /* Remove class active */
        $(this).closest('.page_section').find('.pq_slider__toggle li').removeClass('active');
        $(this).closest('.page_section').find('.pull_quote_new_slider li').removeClass('active');
        $(this).closest('.page_section').find('.testimonial__wrapper').removeClass('active');

        /* Add class active to this toggle and the event that it is associated with */


        $(this).closest('.page_section').find('.pq_slider__toggle li[data-slide="'+slide+'"]').addClass('active');
        $(this).closest('.page_section').find('.pull_quote_new_slider li[data-slide="'+slide+'"]').addClass('active');
        $(this).closest('.page_section').find('.testimonial__wrapper[data-slide="'+slide+'"]').addClass('active');

        if($(this).attr('data-slide') == itemAmt_testimonial_slider - 1) {
          $(this).closest('.page_section').find(".pull_quote_new_slider > ul").animate({
            scrollLeft: 0 + (itemSize * itemAmt_testimonial_slider)
          }, 600);
        } else {
          $(this).closest('.page_section').find(".pull_quote_new_slider > ul").animate({
            scrollLeft: 0 + $('.pull_quote_new_slider > ul li').outerWidth() * slide
          }, 600);
        }

        /* Change current index to this toggle/event */
        currentIndex_testimonial_slider = slide_num;
        cycleItems();
      });







      /* Gallery Sliders  - In Progress */
      $(".section").each(function (index) {
        var currentIndex = 0,
          items = $('.section__img-gallery[data-gallery="'+index+'"] .slider li'),
          itemAmt = items.length;

        function cycleItems() {
          var item = $('.section__img-gallery[data-gallery="'+index+'"] .slider li').eq(currentIndex);
          item.addClass('active');
        }

        $('.section__img-gallery[data-gallery="' + index +'"] .btn__toggle-next').on('click', function (event) {
          event.preventDefault();
          items.removeClass('active');
          currentIndex += 1;
          if (currentIndex > itemAmt - 1) {
            currentIndex = 0;
          }
          cycleItems();
        });

        $('.section__img-gallery[data-gallery="' + index +'"] .btn__toggle-prev').on('click', function (event) {
          event.preventDefault();
          items.removeClass('active');
          currentIndex -= 1;
          if (currentIndex < 0) {
            currentIndex = itemAmt - 1;
          }
          cycleItems();
        });
      });

      $('.section__main-content .color-overlay .video__play').on('click', function(ev) {
        var video = $(this).parent().siblings('iframe');

        var overlay = $(this).parent();
        var overlay_image = $(this).parent().siblings('.overlay');
        var overlay_text = $(this).parent().parent().siblings('.video__caption');

        overlay_image.fadeOut();
        overlay.fadeOut();
        overlay_text.fadeOut();

        video[0].src += "&autoplay=1";
        ev.preventDefault();

      });

      // image grid - in progress
      $('.section__img-grid .grid').masonry({
        columnWidth: '.grid-sizer',
        gutter: '.gutter-sizer',
        itemSelector: '.grid-item',
        percentPosition: true
      });



      // When screen gets to 768 on resize, adjust column 2 video
      $(window).resize(function(){
        if ($(window).width() > 768) {
          $video_section_title = $('.section__main-content .column .stretch').outerHeight();
          $video_section_title = $video_section_title + 20;
          $('.section__main-content .column .title-included').css("margin-top", $video_section_title);
        }
      });
      
      if ($(window).width() > 768) {
        $video_section_title = $('.section__main-content .column .stretch').outerHeight();
        $video_section_title = $video_section_title + 20;
        $('.section__main-content .column .title-included').css("margin-top", $video_section_title);
      }



      /* Accordion JS - NEW */
      $(".accordion .question").on("click", function (e) {
        e.preventDefault();
        var li = $(this).parent();
        var answer = $(this).siblings(".answer");
        var height = answer.children("div").outerHeight();
        $(".accordion >li").removeClass("open");
        li.toggleClass("open");
        answer.toggleClass("open");
        if (answer.hasClass("open")) {
          $(".accordion .answer")
            .removeClass("open")
            .css("max-height", "");
          answer.addClass("open");
          answer.css("max-height", height + "px");
        } else {
          $(".accordion >li").removeClass("open");
          answer.css("max-height", "");
        }
      });











    }
  },

  /*
    Front Page
  */
  home: {
    init: function() {

    

    }
  },

  

  /*
    Postload
  */
  postload: {
    init: function() {

    }
  },

};

// The routing fires all common scripts, followed by the page specific scripts.
// Add additional events for more control over timing e.g. a finalize event
var UTIL = {
  fire: function(func, funcname, args) {
    var namespace = Roots;
    funcname = (funcname === undefined) ? 'init' : funcname;
    if (func !== '' && namespace[func] && typeof namespace[func][funcname] === 'function') {
      namespace[func][funcname](args);
    }
  },
  loadEvents: function() {
    UTIL.fire('preload');

    $.each(document.body.className.replace(/-/g, '_').split(/\s+/),function(i,classnm) {
      UTIL.fire(classnm);
    });

    UTIL.fire('postload');
  }
};

$(document).ready(UTIL.loadEvents);

})(jQuery);

function cycleItems() {
  var item = $('.pq_slider__toggle li[data-slide="'+currentIndex_testimonial_slider+'"]');
  item.addClass('active');
}