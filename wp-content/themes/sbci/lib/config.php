<?php
/*-------------------------------------------------------------------------------
	Enable Theme Support
-------------------------------------------------------------------------------*/

add_theme_support('root-relative-urls');    // Enable relative URLs
add_theme_support('bootstrap-top-navbar');  // Enable Bootstrap's top navbar
add_theme_support('nice-search');           // Enable /?s= to /search/ redirect
add_theme_support('jquery-cdn');            // Enable to load jQuery from the Google CDN
add_theme_support('post-thumbnails');		// Enable featured thumbnails

/*-------------------------------------------------------------------------------
	Configuration Values
-------------------------------------------------------------------------------*/

define('DATE_LOCALE', 			'en_CA' ); 						// Set Locale in PHP for Date Translations (WPML)
define('GOOGLE_ANALYTICS_ID', 	'' ); 							// UA-XXXXX-Y (Note: Universal Analytics only, not Classic Analytics)
define('POST_EXCERPT_LENGTH',	50 ); 							// Length in words for excerpt_length filter (http://codex.wordpress.org/Plugin_API/Filter_Reference/excerpt_length)
define('HIDE_ADMIN_BAR',		true );							// Whether to hide the admin bar on the frontend for logged in users
define('ENABLE_OPTIONS_MENU',	true );						// Whether to enable General Options menu
define('ENABLE_MENUS',			true );							// Whether to enable Menus
define('ENABLE_WIDGETS',		false );						// Whether to enable Widgets
define('ENABLE_COMMENTS',		false );						// Whether to enable Comments
define('ENABLE_POSTS',			false );						// Whether to enable Posts
define('RENAME_BLOG', 			true );							// Whether to rename "Posts" to "Blog"
define('LOGIN_LOGO',			get_template_directory_uri() . '/assets/img/logo-ct.svg' );	// URL to Logo image file for Login screen
define('LOGIN_LOGO_SIZE', 		serialize(array(350, 318)) );	// Logo on Login screen dimensions (width, height)
define('MIN_UPLOAD_DIM',		serialize(array(640, 320)) );	// Enforce minimum dimensions when uploading images (width, height)

/*-------------------------------------------------------------------------------
	Enqueue Scripts
-------------------------------------------------------------------------------*/

function ct_enqueue_scripts() {
	//wp_enqueue_script('jquerypp');
	//wp_enqueue_script('jqueryui');
	//wp_enqueue_script('jqueryui-local');
	//wp_enqueue_script('google-maps');
	//wp_enqueue_script('validation');
	//wp_enqueue_script('selectboxit');
	//wp_enqueue_script('mousewheel');
	//wp_enqueue_script('touchswipe');
	//wp_enqueue_script('modernizr');
}

add_action('wp_enqueue_scripts', 'ct_enqueue_scripts', 101);

/*-------------------------------------------------------------------------------
  Register Sidebars
-------------------------------------------------------------------------------*/

function ct_widgets_init() {
  	register_sidebar(array(
	    'name'          => __('Primary', 'cinnamontoast'),
	    'id'            => 'sidebar-primary',
	    'before_widget' => '<section class="widget %1$s %2$s">',
	    'after_widget'  => '</section>',
	    'before_title'  => '<h3>',
	    'after_title'   => '</h3>',
  	));

  	register_sidebar(array(
	    'name'          => __('Footer', 'cinnamontoast'),
	    'id'            => 'sidebar-footer',
	    'before_widget' => '<section class="widget %1$s %2$s">',
	    'after_widget'  => '</section>',
	    'before_title'  => '<h3>',
	    'after_title'   => '</h3>',
  	));
}

add_action('widgets_init', 'ct_widgets_init');

/*-------------------------------------------------------------------------------
  Register Menus
-------------------------------------------------------------------------------*/

function ct_register_nav() {
	register_nav_menus(array(
	    'primary_navigation' => __('Primary Navigation', 'cinnamontoast'),
		'footer_navigation' => __('Footer Navigation', 'cinnamontoast'),
		'footer_sub_navigation' => __('Secondary Footer Menu', 'cinnamontoast'),
	));
}

add_action('after_setup_theme', 'ct_register_nav');

/*-------------------------------------------------------------------------------
  Register Image Sizes on Theme Activation
-------------------------------------------------------------------------------*/

function add_image_sizes() {
	add_image_size('huge', 3840);
	add_image_size('large', 2560);
	add_image_size('medium', 1920);
	add_image_size('small', 1440);
	add_image_size('tiny', 1024);
	add_image_size('smaller', 768);
	add_image_size('smallest', 416);
	add_image_size('blurry', 204);

	// custom image sizes
	add_image_size('event-home', 458);
	add_image_size('card-home-image', 350, 368);
	add_image_size('card-two-col', 730, 441);
	add_image_size('pl-two-col', 565, 376);
}

add_action('after_setup_theme', 'add_image_sizes');

function ct_remove_default_image_sizes($sizes) {
    unset($sizes['medium']);
    unset($sizes['large']);
    return $sizes;
}
add_filter('intermediate_image_sizes_advanced', 'ct_remove_default_image_sizes');

/*-------------------------------------------------------------------------------
  Modify Administrator Menu
-------------------------------------------------------------------------------*/

function responsive_background($image_id, $size){

  $css;
  $smallest_url;
  $smallest_size;
  $img_srcset = wp_get_attachment_image_srcset($image_id, $size);
  $segments = explode(', ',$img_srcset);
  $classname_id = substr(base_convert(md5($image_id . $size), 16,32), 0, 5);

  foreach($segments as $key => $segment){
    $parts = explode(' ',$segment);
    $segments[$key] = ['url' => $parts[0], 'width' => $parts[1]];
  }

  usort($segments, function($a, $b){
    return intval($a['width']) < intval($b['width']);
  });

  foreach($segments as $key => $segment){
    if($smallest_url == null || intval($segment['width']) < $smallest_size) {
        $smallest_url = $segment['url'];
        $smallest_size = intval($segment['width']);
    }

    $css .= sprintf('@media %1$s (-webkit-min-device-pixel-ratio: 1),%1$s (max-resolution: 192dpi),%4$s (-webkit-min-device-pixel-ratio: 2),%4$s (min-resolution: 192dpi){%3$s:after{background-image: url("%2$s");}}',
    ($key !==0 ? sprintf('(max-width: %1$s) and', (intval($segment['width']) / 1) . 'px') : ''),
    $segment['url'],
    '.resp-bg' .  $classname_id,
    ($key !==0 ? sprintf('(max-width: %1$s) and', (intval($segment['width']) / 2) . 'px') : '')
    );
  }

  $precss = sprintf('%1$s:before {background-image: url("%2$s");}',
  '.resp-bg' .  $classname_id,
  $smallest_url
  );

  return [ 'classname' => 'resp-bg resp-bg' .  $classname_id, 'css' => sprintf('<style>%1$s</style>',$precss . $css)];
}


/*-------------------------------------------------------------------------------
  Modify Administrator Menu
-------------------------------------------------------------------------------*/

function ct_admin_menu() {
	//remove_submenu_page('themes.php');
}

add_filter( 'admin_menu', 'ct_admin_menu', 999 );

/*-------------------------------------------------------------------------------
  TinyMCE Change
-------------------------------------------------------------------------------*/
// Callback function to insert 'styleselect' into the $buttons array
function my_mce_buttons_2( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}
// Register our callback to the appropriate filter
add_filter( 'mce_buttons_2', 'my_mce_buttons_2' );

add_filter('tiny_mce_before_init', 'tiny_mce_remove_unused_formats' );
/*
 * Modify TinyMCE editor to remove H1.
 */
function tiny_mce_remove_unused_formats($init) {
	// Add block format elements you want to show in dropdown
	$init['block_formats'] = 'Paragraph=p;Heading 1=h3;Heading 2=h4;Heading 3=h5;Heading 4=h6;';
	return $init;
}

/*-------------------------------------------------------------------------------
  Create Custom Post Types and Taxonomies
-------------------------------------------------------------------------------*/

function ct_post_types() {

	register_post_type('resources',
	array(
		'labels' => array(
			'name' => __('Resources'),
			'singular_name' => __('Resources'),
			'add_new' => __('Add New'),
			'add_new_item' => __('Add New Resource'),
			'edit' => __('Edit'),
			'edit_item' => __('Edit Resources'),
			'new_item' => __('New Resources'),
			'view' => __('View Resources'),
			'view_item' => __('View Resources'),
			'search_items' => __('Search Resources'),
			'not_found' => __('No Resources found'),
			'not_found_in_trash' => __('No Resources found in Trash'),
			'parent' => __('Parent Resources'),
		),
		'public' => true,
		'supports' => array('title', 'page-attributes', 'thumbnail', 'custom-fields'),
		'menu_icon' => 'dashicons-format-aside',
		'menu_position' => 21,
		'map_meta_cap' => true,
		'hierarchical' => true,
	)
	);

	register_post_type('news-events',
	array(
		'labels' => array(
			'name' => __('News & Events'),
			'singular_name' => __('News & Events'),
			'add_new' => __('Add New'),
			'add_new_item' => __('Add New News & Events'),
			'edit' => __('Edit'),
			'edit_item' => __('Edit News & Events'),
			'new_item' => __('New News & Events'),
			'view' => __('View News & Events'),
			'view_item' => __('View News & Events'),
			'search_items' => __('Search News & Events'),
			'not_found' => __('No News & Events found'),
			'not_found_in_trash' => __('No News & Events found in Trash'),
			'parent' => __('Parent News & Events'),
		),
		'public' => true,
		'supports' => array('title', 'page-attributes', 'thumbnail', 'custom-fields'),
		'menu_icon' => 'dashicons-testimonial',
		'menu_position' => 21,
		'map_meta_cap' => true,
		'hierarchical' => true,
	)
	);

	register_post_type('accordion',
	array(
		'labels' => array(
			'name' => __('Accordion'),
			'singular_name' => __('Accordion'),
			'add_new' => __('Add New'),
			'add_new_item' => __('Add New Accordion Item'),
			'edit' => __('Edit'),
			'edit_item' => __('Edit Accordions'),
			'new_item' => __('New Accordion Item'),
			'view' => __('View Accordions'),
			'view_item' => __('View Accordion Item'),
			'search_items' => __('Search Accordions'),
			'not_found' => __('No Accordion Items found'),
			'not_found_in_trash' => __('No Accordion Items found in Trash'),
			'parent' => __('Parent Accordion'),
		),
		'public' => true,
		'supports' => array('title', 'page-attributes', 'thumbnail', 'custom-fields'),
		'menu_icon' => 'dashicons-list-view',
		'menu_position' => 21,
		'map_meta_cap' => true,
		'hierarchical' => true,
	)
	);

	register_post_type('people',
	array(
		'labels' => array(
			'name' => __('People'),
			'singular_name' => __('People'),
			'add_new' => __('Add New'),
			'add_new_item' => __('Add New People Item'),
			'edit' => __('Edit'),
			'edit_item' => __('Edit People'),
			'new_item' => __('New People Item'),
			'view' => __('View People'),
			'view_item' => __('View People Item'),
			'search_items' => __('Search People'),
			'not_found' => __('No People Items found'),
			'not_found_in_trash' => __('No People Items found in Trash'),
			'parent' => __('Parent People'),
		),
		'public' => true,
		'supports' => array('title', 'page-attributes', 'thumbnail', 'custom-fields'),
		'menu_icon' => 'dashicons-groups',
		'menu_position' => 21,
		'map_meta_cap' => true,
		'hierarchical' => true,
	)
	);


    register_taxonomy('people-types',
        array('people'),
        array(
            'label' => __('People Types'),
            'rewrite' => array('slug' => 'people-types'),
            'hierarchical' => true,
        )
    );
	
	/*
	register_post_type( 'news',
		array(
			'labels' => array(
				'name' => __( 'News' ),
				'singular_name' => __( 'News' ),
				'add_new' => __( 'Add New' ),
				'add_new_item' => __( 'Add New Article' ),
				'edit' => __( 'Edit' ),
				'edit_item' => __( 'Edit News' ),
				'new_item' => __( 'New News' ),
				'view' => __( 'View News' ),
				'view_item' => __( 'View News' ),
				'search_items' => __( 'Search News' ),
				'not_found' => __( 'No News found' ),
				'not_found_in_trash' => __( 'No News found in Trash' ),
				'parent' => __( 'Parent News' ),
			),
			'public' => true,
			'supports' => array('title', 'page-attributes', 'thumbnail'),
			'menu_icon' => 'dashicons-format-chat',
			'menu_position' => 21,
			'map_meta_cap' => true,
			'hierarchical' => true
		)
	);

	register_taxonomy( 'categories',
		array('news'),
		array(
			'label' => __( 'Categories' ),
			'rewrite' => array( 'slug' => 'categories' ),
			'hierarchical' => true,
		)
	);
	*/
}

add_action( 'init', 'ct_post_types', 99 );
?>
