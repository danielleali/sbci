<?php
/* ==========================================================================
   Move yoast meta boxes to the bottom
   ========================================================================== */
    add_filter('wpseo_metabox_prio', function () {return 'low';});

    add_filter('gform_validation_message', 'change_message', 10, 2);
    function change_message($message, $form) {
        // return "<div class='validation_error'>Failed Validation - " . $form['title'] . '</div>';
        return "";
    }

    add_filter('gform_field_validation', 'custom_validation', 10, 4);
    function custom_validation($message) {
        $message['message'] = 'Please ensure all required fields are filled before submitting.';
        return $message;
    }

    function my_acf_init() {
        acf_update_setting('google_api_key', 'AIzaSyAH7mmRWEB_jRGP1IgUwaer_s4Yv2c6Zbc');
    }

    add_action('acf/init', 'my_acf_init');


/* ==========================================================================
   Hides the default editor on pages
   ========================================================================== */
    add_action( 'admin_init', 'hide_editor' );
 
    function hide_editor() {

        remove_post_type_support('page', 'editor');
        add_post_type_support('accordion', 'editor'); // Add back to Accordions/FAQs
    }



/* ==========================================================================
   Programmatically assign taxonomy term by custom fields - NOT WORKING YET
   @param int $post_id
   ========================================================================== */
    // function rd_assign_taxonomies($post_id) {

    //     if (get_post_type($post_id) == 'people') {
    //         $terms = array();

    //         // Here we'd check to see if the post has the specific fields, and if so add the IDs or slugs of the taxonomy terms to the $terms array
    //         if (get_field('type') == 'board-of-directors') {
    //             $terms = 'board-of-directors';
    //         }
    //         if (get_field('type') == 'staff') {
    //             $terms = 'staff';
    //         }
    //         if (get_field('type') == 'consultants') {
    //             $terms = 'consultants';
    //         }
    //         // The exact details will depend on how the checkboxes were implemented, for example as native postmeta or ACF fields

    //         wp_set_object_terms( $post_id, $terms, 'people-types', false );
    //     }
    // }

    // add_action('save_post', 'rd_assign_taxonomies');


    /* =========================================================================================
    Display a custom taxonomy dropdown in admin
    @author Mike Hemberger
    @link http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
   ========================================================================================= */
    add_action('restrict_manage_posts', 'tsm_filter_post_type_by_taxonomy');
    function tsm_filter_post_type_by_taxonomy() {
        global $typenow;
        $post_type = 'people'; // change to your post type
        $taxonomy  = 'people-types'; // change to your taxonomy
        if ($typenow == $post_type) {
            $selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
            $info_taxonomy = get_taxonomy($taxonomy);
            wp_dropdown_categories(array(
                'show_option_all' => sprintf( __( 'Show all %s', 'textdomain' ), $info_taxonomy->label ),
                'taxonomy'        => $taxonomy,
                'name'            => $taxonomy,
                'orderby'         => 'name',
                'selected'        => $selected,
                'show_count'      => true,
                'hide_empty'      => true,
            ));
        };
    }


/* =========================================================================================
    Filter posts by taxonomy in admin
    @author  Mike Hemberger
    @link http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
   ========================================================================================= */

    add_filter('parse_query', 'tsm_convert_id_to_term_in_query');
    function tsm_convert_id_to_term_in_query($query) {
        global $pagenow;
        $post_type = 'people'; // change to your post type
        $taxonomy  = 'people-types'; // change to your taxonomy
        $q_vars    = &$query->query_vars;
        if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
            $term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
            $q_vars[$taxonomy] = $term->slug;
        }
    }

?>