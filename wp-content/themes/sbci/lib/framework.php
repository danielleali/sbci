<?php
register_activation_hook( __FILE__, 'ct_flush_rewrites' );

function ct_flush_rewrites() {
  flush_rewrite_rules();
}

/*-------------------------------------------------------------------------------
  Enqueue scripts and stylesheets
-------------------------------------------------------------------------------*/

function ct_scripts() {
  wp_enqueue_style('main', get_template_directory_uri() . '/assets/css/main.min.css', false, null);

  wp_deregister_script('jquery');

  // CDN //
  wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js', array(), null, false);
  wp_register_script('jquerypp', '//cdn.jsdelivr.net/jquerypp/1.0.1/jquerypp.min.js', array('jquery'), null, true);
  wp_register_script('jqueryui', '//code.jquery.com/ui/1.11.4/jquery-ui.min.js', array('jquery'), null, true);
  wp_register_script('jqueryui-local', '//ajax.googleapis.com/ajax/libs/jqueryui/1.11.0/i18n/jquery-ui-i18n.min.js', array('jquery'), null, true);
  wp_register_script('google-maps', '//maps.googleapis.com/maps/api/js?v=3.exp&sensor=false', array('jquery'), null, true);
  wp_register_script('validation', '//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js', array('jquery'), null, true);
  wp_register_script('bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js', array('jquery'), null, true);
  wp_register_script('validation', '//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js', array('jquery'), null, true);
  wp_register_script('modernizr', '//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js', array('jquery'), null, true);

  // Local //
  wp_register_script('selectboxit', get_template_directory_uri() . '/assets/js/plugins/selectboxit.min.js', array('jquery'), null, true);
  wp_register_script('mousewheel', get_template_directory_uri() . '/assets/js/plugins/mousewheel.min.js', array('jquery'), null, true);
  wp_register_script('touchswipe', get_template_directory_uri() . '/assets/js/plugins/touchswipe.min.js', array('jquery'), null, true);

  // Always Included //
  wp_enqueue_script('jquery');
}

add_action('wp_enqueue_scripts', 'ct_scripts', 100);

function ct_main_script() {
  wp_register_script('main', get_template_directory_uri() . '/assets/js/main.min.js', array(), null, true);
  wp_enqueue_script('main');
}

add_action('wp_enqueue_scripts', 'ct_main_script', 102);

/*-------------------------------------------------------------------------------
  Admin Styles
-------------------------------------------------------------------------------*/

function ct_admin_style() {
  wp_enqueue_style( 'admin-style', get_template_directory_uri() . '/assets/css/admin.min.css' );
}

add_action( 'admin_head', 'ct_admin_style' );

/*-------------------------------------------------------------------------------
  Force No Toolbar on Frontend
-------------------------------------------------------------------------------*/

if (HIDE_ADMIN_BAR) {
  add_filter('show_admin_bar', '__return_false');
}

/*-------------------------------------------------------------------------------
  Set Locale in PHP for Date Translations (WPML)
-------------------------------------------------------------------------------*/

if (DATE_LOCALE) {
  setlocale(LC_ALL, DATE_LOCALE);
} else {
  $lang = str_replace("-", "_", get_bloginfo('language'));
  setlocale(LC_ALL, $lang);
}

/*-------------------------------------------------------------------------------
  Allow date formats to be translatable
-------------------------------------------------------------------------------*/

function translate_date_format($format) {
    if (function_exists('get_the_date')) {
        if(function_exists('icl_register_string')){
          icl_register_string('Formats', 'Date Format', $format);
          $format = icl_translate('Formats', 'Date Format', $format);
        }
    }
    return $format;
}
add_filter('option_date_format', 'translate_date_format');

/*-------------------------------------------------------------------------------
  Enforce Minimum Dimensions for Image Uploads
-------------------------------------------------------------------------------*/

function ct_block_authors_from_uploading_small_images() {
  if ( !current_user_can( 'administrator') && MIN_UPLOAD_DIM) {
    add_filter( 'wp_handle_upload_prefilter', 'ct_block_small_images_upload' );
  }
}

function ct_block_small_images_upload( $file ) {
  $mimes = array( 'image/jpeg', 'image/png', 'image/gif' );

  if ( !in_array( $file['type'], $mimes ) ) {
    return $file;
  }

  $img = getimagesize( $file['tmp_name'] );
  if (MIN_UPLOAD_DIM) {
    $dimensions = unserialize(MIN_UPLOAD_DIM);
  } else {
    $dimensions = array(0, 0);
  }
  $minimum = array( 'width' => $dimensions[0], 'height' => $dimensions[1] );

  if ( $img[0] < $minimum['width'] ) {
    $file['error'] = 'Image is too small. Minimum width is ' . $minimum['width'] . 'px. Uploaded image width is ' . $img[0] . 'px';
  } else if ( $img[1] < $minimum['height'] ) {
    $file['error'] = 'Image is too small. Minimum height is ' . $minimum['height'] . 'px. Uploaded image height is ' . $img[1] . 'px';
  }
  return $file;
}

add_action( 'admin_init', 'ct_block_authors_from_uploading_small_images' );

/*-------------------------------------------------------------------------------
  Set Default Admin Panel Post Ordering
-------------------------------------------------------------------------------*/

function ct_set_custom_post_types_admin_order($wp_query) {
  if (is_admin()) {
    $post_type = $wp_query->query['post_type'];
    $orderby = $wp_query->get( 'orderby');
    if ( !$orderby  && ($post_type == 'page') ) {
      $wp_query->set('orderby', 'menu_order');
      $wp_query->set('order', 'ASC');
    } else if ( $orderby == 'menu_order title' ) {
      $wp_query->set('order', 'ASC');
    }
  }
}
add_filter( 'pre_get_posts', 'ct_set_custom_post_types_admin_order' );

function ct_custom_page_limit($number) {
  return 5;
}

add_filter( 'simple_page_ordering_limit', 'ct_custom_page_limit' );


/*-------------------------------------------------------------------------------
  Thumbnail Helper
-------------------------------------------------------------------------------*/

function the_thumbnail($post_id = null, $size = 'large') {
  $thumbnail = get_the_thumbnail($post_id, $size); ?>
  <div class="thumb">
  <?php if ($thumbnail) : ?>
    <img src="<?php echo $thumbnail['url']; ?>" alt="<?php echo $thumbnail['caption']; ?>" />
  <?php else : ?>
    <img src="<?php bloginfo('url'); ?>/assets/img/default.jpg" alt="">
  <?php endif; ?>
  </div>
  <?php
}

function get_the_thumbnail($post_id, $size) {
  global $post;
  if ($post_id == null) $post_id = $post->ID;
  $thumb_id = get_post_thumbnail_id($post_id);
  if ($thumb_id != null) {
    $thumb_url = wp_get_attachment_image_src($thumb_id, $size, true);
    $thumbnail_image = get_post($thumb_id);
    if (!empty($thumbnail_image)) {
      $thumbnail['caption'] = $thumbnail_image->post_excerpt;
      $thumbnail['url'] = $thumb_url[0];
      return $thumbnail;
    } else {
      return false;
    }
  }
  return false;
}

/*-------------------------------------------------------------------------------
  Enable Admin Menu Sections from Appearance as Top Level Menus
-------------------------------------------------------------------------------*/

function ct_enable_widgets() {
  global $menu, $pagenow;
  if (ENABLE_WIDGETS) {
    add_menu_page('Widgets', 'Widgets', 'manage_options', 'widgets.php', null, 'dashicons-welcome-widgets-menus' , 55);
  }
  if (ENABLE_MENUS) {
    add_menu_page('Menus', 'Menus', 'manage_options', 'nav-menus.php', null, 'dashicons-menu', 56);
  }
  if (!ENABLE_POSTS) {
    remove_menu_page('edit.php');
  }

  // Fix for Appearance Top-Level Menu Highlight //
  if ($pagenow == 'widgets.php') {
    $menu[60][4] = 'menu-top menu-icon-appearance nohighlight';
  } else {
    $menu[60][4] = 'menu-top menu-icon-appearance';
  }
}

add_filter( 'admin_menu', 'ct_enable_widgets', 150 );




/*-------------------------------------------------------------------------------
  Remove Comments from Backend
-------------------------------------------------------------------------------*/

function ct_my_page_edit_columns($columns) {
  unset($columns['comments']);
  return $columns;
}

function ct_post_edit_columns($columns) {
  unset($columns['comments']);
  return $columns;
}

function ct_remove_comment_menu() {
  remove_menu_page( 'edit-comments.php' );
}

if (!ENABLE_COMMENTS) {
  add_filter('manage_edit-page_columns', 'ct_my_page_edit_columns');
  add_filter('manage_edit-post_columns', 'ct_post_edit_columns');
  add_action('admin_menu', 'ct_remove_comment_menu');
}

/*-------------------------------------------------------------------------------
  Remove Appearance Menu Subitems from Admin Panel
-------------------------------------------------------------------------------*/

function ct_remove_appearance_menus() {
  global $submenu, $menu;
  remove_submenu_page('themes.php', 'themes.php');
  remove_submenu_page('themes.php', 'customize.php');
  remove_submenu_page('themes.php', 'widgets.php');
  remove_submenu_page('themes.php', 'nav-menus.php');
  unset($submenu['themes.php'][6]);

}

add_action( 'admin_menu', 'ct_remove_appearance_menus', 140 );

function ct_remove_apperance_editor() {
  define('DISALLOW_FILE_EDIT', true);
}

add_action( 'init', 'ct_remove_apperance_editor', 160);

/*-------------------------------------------------------------------------------
  Date Helpers
-------------------------------------------------------------------------------*/

function the_datetime($timestamp, $datecode) {
  echo get_datetime($timestamp, $datecode);
}

function get_datetime($timestamp, $datecode) {
  if ($datecode == null) {
    $datecode = '%A, %B %e, %Y';
  }
  return utf8_encode(strftime($datecode, $timestamp));
}

/*-------------------------------------------------------------------------------
  Add Options Page
-------------------------------------------------------------------------------*/

if ( function_exists('acf_add_options_page') && ENABLE_OPTIONS_MENU ) {
  acf_add_options_page(array(
    'page_title'  => 'General Options',
    'menu_title'  => 'General Options',
    'menu_slug'   => 'sm-general-settings',
    'capability'  => 'edit_theme_options',
    'redirect'    => false,
    'icon_url'    => 'dashicons-share'
  ));
}

/*-------------------------------------------------------------------------------
  Rename Posts to Blog
-------------------------------------------------------------------------------*/

function ct_revcon_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Blog';
    $submenu['edit.php'][5][0] = 'Blog';
    $submenu['edit.php'][10][0] = 'Add New Blog';
    $submenu['edit.php'][16][0] = 'Blog Tags';
}

function revcon_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Blog';
    $labels->singular_name = 'Blog';
    $labels->add_new = 'Add New Blog';
    $labels->add_new_item = 'Add New Blog';
    $labels->edit_item = 'Edit Blog';
    $labels->new_item = 'Blog';
    $labels->view_item = 'View Blog';
    $labels->search_items = 'Search Blog';
    $labels->not_found = 'No Blog articles found';
    $labels->not_found_in_trash = 'No Blog articles found in Trash';
    $labels->all_items = 'All Blog articles';
    $labels->menu_name = 'Blog';
    $labels->name_admin_bar = 'Blog';
}

if (RENAME_BLOG) {
  add_action( 'admin_menu', 'ct_revcon_change_post_label' );
  add_action( 'init', 'revcon_change_post_object' );
}

/*-------------------------------------------------------------------------------
  Theme Wrapper
-------------------------------------------------------------------------------*/

function ct_template_path() {
  return CT_Wrapping::$main_template;
}

class CT_Wrapping {
  static $main_template;
  static $base;

  public function __construct($template = 'base.php') {
    $this->slug = basename($template, '.php');
    $this->templates = array($template);

    if (self::$base) {
      $str = substr($template, 0, -4);
      array_unshift($this->templates, sprintf($str . '-%s.php', self::$base));
    }
  }

  public function __toString() {
    $this->templates = apply_filters('ct_wrap_' . $this->slug, $this->templates);
    return locate_template($this->templates);
  }

  static function wrap($main) {
    self::$main_template = $main;
    self::$base = basename($main, '.php');

    if (self::$base === 'index') {
      self::$base = false;
    }

    return new CT_Wrapping();
  }
}

add_filter('template_include', array('CT_Wrapping', 'wrap'), 99);


/*-------------------------------------------------------------------------------
  Utility Functions
-------------------------------------------------------------------------------*/

function add_filters($tags, $function) {
  foreach ($tags as $tag) {
    add_filter($tag, $function);
  }
}

function is_element_empty($element) {
  $element = trim($element);
  return empty($element) ? false : true;
}

/*-------------------------------------------------------------------------------
  Page Title
-------------------------------------------------------------------------------*/

function ct_title() {
  if (is_home()) {
    if (get_option('page_for_posts', true)) {
      return get_the_title(get_option('page_for_posts', true));
    } else {
      return __('Latest Posts', 'cinnamontoast');
    }
  } elseif (is_archive()) {
    $term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
    if ($term) {
      return apply_filters('single_term_title', $term->name);
    } elseif (is_post_type_archive()) {
      return apply_filters('the_title', get_queried_object()->labels->name);
    } elseif (is_day()) {
      return sprintf(__('Daily Archives: %s', 'cinnamontoast'), get_the_date());
    } elseif (is_month()) {
      return sprintf(__('Monthly Archives: %s', 'cinnamontoast'), get_the_date('F Y'));
    } elseif (is_year()) {
      return sprintf(__('Yearly Archives: %s', 'cinnamontoast'), get_the_date('Y'));
    } elseif (is_author()) {
      $author = get_queried_object();
      return sprintf(__('Author Archives: %s', 'cinnamontoast'), $author->display_name);
    } else {
      return single_cat_title('', false);
    }
  } elseif (is_search()) {
    return sprintf(__('Search Results for %s', 'cinnamontoast'), get_search_query());
  } elseif (is_404()) {
    return __('Not Found', 'cinnamontoast');
  } else {
    return get_the_title();
  }
}

function ct_wp_title($title) {
  if (is_feed()) {
    return $title;
  }

  $title .= get_bloginfo('name');

  return $title;
}

add_filter('wp_title', 'ct_wp_title', 10);

/*-------------------------------------------------------------------------------
  Root Relative URLs
-------------------------------------------------------------------------------*/

function ct_root_relative_url($input) {
  preg_match('|https?://([^/]+)(/.*)|i', $input, $matches);

  if (!isset($matches[1]) || !isset($matches[2])) {
    return $input;
  } elseif (($matches[1] === $_SERVER['SERVER_NAME']) || $matches[1] === $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT']) {
    return wp_make_link_relative($input);
  } else {
    return $input;
  }
}

function ct_enable_root_relative_urls() {
  return !(is_admin() || in_array($GLOBALS['pagenow'], array('wp-login.php', 'wp-register.php'))) && current_theme_supports('root-relative-urls');
}

if (ct_enable_root_relative_urls()) {
  $root_rel_filters = array(
    'bloginfo_url',
    'the_permalink',
    'wp_list_pages',
    'wp_list_categories',
    'ct_wp_nav_menu_item',
    'the_content_more_link',
    'the_tags',
    'get_pagenum_link',
    'get_comment_link',
    'month_link',
    'day_link',
    'year_link',
    'tag_link',
    'the_author_posts_link',
    'script_loader_src',
    'style_loader_src'
  );

  add_filters($root_rel_filters, 'ct_root_relative_url');
}

// /*-------------------------------------------------------------------------------
//   Navigation Walker
// -------------------------------------------------------------------------------*/

// class CT_Nav_Walker extends Walker_Nav_Menu {
//   function check_current($classes) {
//     return preg_match('/(current[-_])|active|dropdown/', $classes);
//   }

//   function start_lvl(&$output, $depth = 0, $args = array()) {
//     if ($depth === 1) {
//       $output .= "\n<ul class=\"dropdown-menu dropdown-menu__secondary\">\n";
//     } elseif ($depth === 2) {
//       $output .= "\n<ul class=\"dropdown-menu dropdown-menu__tertiary\">\n";
//     } else {
//       $output .= "\n<ul class=\"dropdown-menu dropdown-menu__primary\">\n";
//     }
//   }
//   // function start_lvl(&$output, $depth = 0, $args = array()) {
//   //   $output .= "\n<ul class=\"dropdown-menu\">\n";
//   // }

//   // function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
//   //   $item_html = '';
//   //   parent::start_el($item_html, $item, $depth, $args);

//   //   if ($item->is_dropdown && ($depth === 0)) {
//   //     $item_html = str_replace('<a', '<a class="dropdown-toggle" data-toggle="dropdown" data-target="#"', $item_html);
//   //   }
//   //   elseif (stristr($item_html, 'li class="divider')) {
//   //     $item_html = preg_replace('/<a[^>]*>.*?<\/a>/iU', '', $item_html);
//   //   }
//   //   elseif (stristr($item_html, 'li class="dropdown-header')) {
//   //     $item_html = preg_replace('/<a[^>]*>(.*)<\/a>/iU', '$1', $item_html);
//   //   }

//   //   $item_html = apply_filters('ct_wp_nav_menu_item', $item_html);
//   //   $output .= $item_html;
//   // }
//   function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
//     $item_html = '';
//     parent::start_el($item_html, $item, $depth, $args);

//     if ($item->is_dropdown && ($depth === 0)) {
//       $item_html = str_replace('<a', '<a class="dropdown-toggle" data-toggle="dropdown" data-target="#"', $item_html);
//     }
//     elseif ($item->is_dropdown && ($depth === 1) || $item->is_dropdown && ($depth === 2)) {
//       $item_html = str_replace('<a', '<a class="inner-dropdown-toggle" data-toggle="dropdown" data-target="#"', $item_html);
//     }
//     elseif (stristr($item_html, 'li class="divider')) {
//       $item_html = preg_replace('/<a[^>]*>.*?<\/a>/iU', '', $item_html);
//     }
//     elseif (stristr($item_html, 'li class="dropdown-header')) {
//       $item_html = preg_replace('/<a[^>]*>(.*)<\/a>/iU', '$1', $item_html);
//     }
    
//     $item_html = apply_filters('ct_wp_nav_menu_item', $item_html);
//     $output .= $item_html;
//   }

//   function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output) {
//     $element->is_dropdown = ((!empty($children_elements[$element->ID]) && (($depth + 1) < $max_depth || ($max_depth === 0))));

//     if ($element->is_dropdown) {
//       $element->classes[] = 'dropdown';
//     }

//     parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
//   }
// }

// function ct_nav_menu_css_class($classes, $item) {
//   $slug = sanitize_title($item->title);
//   $classes = preg_replace('/(current(-menu-|[-_]page[-_])(item|parent|ancestor))/', 'active', $classes);
//   $classes = preg_replace('/^((menu|page)[-_\w+]+)+/', '', $classes);

//   $classes[] = 'menu-' . $slug;

//   $classes = array_unique($classes);

//   return array_filter($classes, 'is_element_empty');
// }

// function ct_nav_menu_id($menu_id, $item, $args) {
//   if ($args->menu_id == 'menu-primary-navigation') {
//     $slug = 'menu-'.sanitize_title($item->title);
//   } else {
//     $slug = $menu_id;
//   }

//   return $slug;
// }

// add_filter('nav_menu_css_class', 'ct_nav_menu_css_class', 10, 2);
// add_filter('nav_menu_item_id', 'ct_nav_menu_id', 10, 3);

// function ct_nav_menu_args($args = '') {
//   $ct_nav_menu_args['container'] = false;

//   if (!$args['items_wrap']) {
//     $ct_nav_menu_args['items_wrap'] = '<ul class="%2$s">%3$s</ul>';
//   }

//   if (current_theme_supports('bootstrap-top-navbar') && !$args['depth']) {
//     $ct_nav_menu_args['depth'] = 2;
//   }

//   if (!$args['walker']) {
//     $ct_nav_menu_args['walker'] = new CT_Nav_Walker();
//   }

//   return array_merge($args, $ct_nav_menu_args);
// }
// add_filter('wp_nav_menu_args', 'ct_nav_menu_args');


// function ct_attachment_link_class($html) {
//   $postid = get_the_ID();
//   $html = str_replace('<a', '<a class="thumbnail img-thumbnail"', $html);
//   return $html;
// }
// add_filter('wp_get_attachment_link', 'ct_attachment_link_class', 10, 1);

/*-------------------------------------------------------------------------------
  Navigation Walker
-------------------------------------------------------------------------------*/
class CT_Nav_Walker extends Walker_Nav_Menu {
  function check_current($classes) {
    return preg_match('/(current[-_])|active|dropdown/', $classes);
  }
  function start_lvl(&$output, $depth = 0, $args = array()) {
    if ($depth === 1) {
      $output .= "\n<ul class=\"dropdown-menu dropdown-menu__secondary\">\n";
    } elseif ($depth === 2) {
      $output .= "\n<ul class=\"dropdown-menu dropdown-menu__tertiary\">\n";
    } else {
      $output .= "\n<ul class=\"dropdown-menu dropdown-menu__primary\">\n";
    }
  }
  function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
    $item_html = '';
    parent::start_el($item_html, $item, $depth, $args);
    if ($item->is_dropdown && ($depth === 0)) {
      $item_html = str_replace('<a', '<a class="dropdown-toggle" data-toggle="dropdown" data-target="#"', $item_html);
    }
    elseif ($item->is_dropdown && ($depth === 1) || $item->is_dropdown && ($depth === 2)) {
      $item_html = str_replace('<a', '<a class="inner-dropdown-toggle" data-toggle="dropdown" data-target="#"', $item_html);
    }
    elseif (stristr($item_html, 'li class="divider')) {
      $item_html = preg_replace('/<a[^>]*>.*?<\/a>/iU', '', $item_html);
    }
    elseif (stristr($item_html, 'li class="dropdown-header')) {
      $item_html = preg_replace('/<a[^>]*>(.*)<\/a>/iU', '$1', $item_html);
    }
    $item_html = apply_filters('ct_wp_nav_menu_item', $item_html);
    $output .= $item_html;
  }
  function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output) {
    $element->is_dropdown = ((!empty($children_elements[$element->ID]) && (($depth + 1) < $max_depth || ($max_depth === 0))));
    if ($element->is_dropdown) {
      $element->classes[] = 'dropdown';
    }
    parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
  }
}
function ct_nav_menu_css_class($classes, $item) {
  $slug = sanitize_title($item->title);
  $classes = preg_replace('/(current(-menu-|[-_]page[-_])(item|parent|ancestor))/', 'active', $classes);
  $classes = preg_replace('/^((menu|page)[-_\w+]+)+/', '', $classes);
  $classes[] = 'menu-' . $slug;
  $classes = array_unique($classes);
  return array_filter($classes, 'is_element_empty');
}
function ct_nav_menu_id($menu_id, $item, $args) {
  if ($args->menu_id == 'menu-primary-navigation') {
    $slug = 'menu-'.sanitize_title($item->title);
  } else {
    $slug = $menu_id;
  }
  return $slug;
}
add_filter('nav_menu_css_class', 'ct_nav_menu_css_class', 10, 2);
add_filter('nav_menu_item_id', 'ct_nav_menu_id', 10, 3);
function ct_nav_menu_args($args = '') {
  $ct_nav_menu_args['container'] = false;
  if (!$args['items_wrap']) {
    $ct_nav_menu_args['items_wrap'] = '<ul class="%2$s">%3$s</ul>';
  }
  if (current_theme_supports('bootstrap-top-navbar') && !$args['depth']) {
    $ct_nav_menu_args['depth'] = 4;
  }
  if (!$args['walker']) {
    $ct_nav_menu_args['walker'] = new CT_Nav_Walker();
  }
  return array_merge($args, $ct_nav_menu_args);
}
add_filter('wp_nav_menu_args', 'ct_nav_menu_args');
function ct_attachment_link_class($html) {
  $postid = get_the_ID();
  $html = str_replace('<a', '<a class="thumbnail img-thumbnail"', $html);
  return $html;
}
add_filter('wp_get_attachment_link', 'ct_attachment_link_class', 10, 1);

/*-------------------------------------------------------------------------------
  Head Cleanup
-------------------------------------------------------------------------------*/

function ct_head_cleanup() {
  remove_action('wp_head', 'feed_links', 2);
  remove_action('wp_head', 'feed_links_extra', 3);
  remove_action('wp_head', 'rsd_link');
  remove_action('wp_head', 'wlwmanifest_link');
  remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
  remove_action('wp_head', 'wp_generator');
  remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

  global $wp_widget_factory;
  remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));

  if (!class_exists('WPSEO_Frontend')) {
    remove_action('wp_head', 'rel_canonical');
    add_action('wp_head', 'ct_rel_canonical');
  }
}

function ct_rel_canonical() {
  global $wp_the_query;

  if (!is_singular()) {
    return;
  }

  if (!$id = $wp_the_query->get_queried_object_id()) {
    return;
  }

  $link = get_permalink($id);
  echo "\t<link rel=\"canonical\" href=\"$link\">\n";
}

add_action('init', 'ct_head_cleanup');

/*-------------------------------------------------------------------------------
  Remove Wordpress Version from RSS
-------------------------------------------------------------------------------*/

add_filter('the_generator', '__return_false');

/*-------------------------------------------------------------------------------
  Clean up language_attributes()
-------------------------------------------------------------------------------*/

function ct_language_attributes() {
  $attributes = array();
  $output = '';

  if (is_rtl()) {
    $attributes[] = 'dir="rtl"';
  }

  $lang = get_bloginfo('language');

  if ($lang) {
    $attributes[] = "lang=\"$lang\"";
  }

  $output = implode(' ', $attributes);
  $output = apply_filters('ct_language_attributes', $output);

  return $output;
}

add_filter('language_attributes', 'ct_language_attributes');


/*-------------------------------------------------------------------------------
  Add and remove body_class() classes
-------------------------------------------------------------------------------*/

function ct_body_class($classes) {
  if (is_single() || is_page() && !is_front_page()) {
    $classes[] = basename(get_permalink());
  }

  $home_id_class = 'page-id-' . get_option('page_on_front');
  $remove_classes = array(
    'page-template-default',
    $home_id_class
  );
  $classes = array_diff($classes, $remove_classes);

  return $classes;
}

add_filter('body_class', 'ct_body_class');

/*-------------------------------------------------------------------------------
  Add Bootstrap thumbnail styling to images with captions
-------------------------------------------------------------------------------*/

function ct_caption($output, $attr, $content) {
  if (is_feed()) {
    return $output;
  }

  $defaults = array(
    'id'      => '',
    'align'   => 'alignnone',
    'width'   => '',
    'caption' => ''
  );

  $attr = shortcode_atts($defaults, $attr);

  if ($attr['width'] < 1 || empty($attr['caption'])) {
    return $content;
  }

  $attributes  = (!empty($attr['id']) ? ' id="' . esc_attr($attr['id']) . '"' : '' );
  $attributes .= ' class="thumbnail wp-caption ' . esc_attr($attr['align']) . '"';
  $attributes .= ' style="width: ' . (esc_attr($attr['width']) + 10) . 'px"';

  $output  = '<figure' . $attributes .'>';
  $output .= do_shortcode($content);
  $output .= '<figcaption class="caption wp-caption-text">' . $attr['caption'] . '</figcaption>';
  $output .= '</figure>';

  return $output;
}

add_filter('img_caption_shortcode', 'ct_caption', 10, 3);

/*-------------------------------------------------------------------------------
  Remove unnecessary dashboard widgets
-------------------------------------------------------------------------------*/

function ct_remove_dashboard_widgets() {
  remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');
  remove_meta_box('dashboard_plugins', 'dashboard', 'normal');
  remove_meta_box('dashboard_primary', 'dashboard', 'normal');
  remove_meta_box('dashboard_secondary', 'dashboard', 'normal');
}

add_action('admin_init', 'ct_remove_dashboard_widgets');

/*-------------------------------------------------------------------------------
  Clean up the_excerpt()
-------------------------------------------------------------------------------*/

function ct_excerpt_length($length) {
  return POST_EXCERPT_LENGTH;
}

function ct_excerpt_more($more) {
  return '...';
}

add_filter('excerpt_length', 'ct_excerpt_length');
add_filter('excerpt_more', 'ct_excerpt_more');

/*-------------------------------------------------------------------------------
  Remove unnecessary self-closing tags
-------------------------------------------------------------------------------*/

function ct_remove_self_closing_tags($input) {
  return str_replace(' />', '>', $input);
}

add_filter('get_avatar',          'ct_remove_self_closing_tags'); // <img />
add_filter('comment_id_fields',   'ct_remove_self_closing_tags'); // <input />
add_filter('post_thumbnail_html', 'ct_remove_self_closing_tags'); // <img />


/*-------------------------------------------------------------------------------
  Redirects search results from /?s=query to /search/query/
-------------------------------------------------------------------------------*/

function ct_nice_search_redirect() {
  global $wp_rewrite;
  if (!isset($wp_rewrite) || !is_object($wp_rewrite) || !$wp_rewrite->using_permalinks()) {
    return;
  }

  $search_base = $wp_rewrite->search_base;
  if (is_search() && !is_admin() && strpos($_SERVER['REQUEST_URI'], "/{$search_base}/") === false) {
    wp_redirect(home_url("/{$search_base}/" . urlencode(get_query_var('s'))));
    exit();
  }
}

if (current_theme_supports('nice-search')) {
  add_action('template_redirect', 'ct_nice_search_redirect');
}

/*-------------------------------------------------------------------------------
  Fix for empty search queries redirecting to home page
-------------------------------------------------------------------------------*/

function ct_request_filter($query_vars) {
  if (isset($_GET['s']) && empty($_GET['s']) && !is_admin()) {
    $query_vars['s'] = ' ';
  }

  return $query_vars;
}
add_filter('request', 'ct_request_filter');

/*-------------------------------------------------------------------------------
  Tell WordPress to use searchform.php from the templates/ directory
-------------------------------------------------------------------------------*/

function ct_get_search_form($form) {
  $form = '';
  locate_template('/templates/searchform.php', true, false);
  return $form;
}

add_filter('get_search_form', 'ct_get_search_form');

/*-------------------------------------------------------------------------------
  Initialize
-------------------------------------------------------------------------------*/

function ct_setup() {
  load_theme_textdomain('cinnamontoast', get_template_directory() . '/lang');
  add_editor_style(get_template_directory_uri() . '/assets/css/editor-style.css');
}

add_action('after_setup_theme', 'ct_setup');

/*-------------------------------------------------------------------------------
  Include Google Analytics
-------------------------------------------------------------------------------*/

function ct_google_analytics() { ?>
<script>
  (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
  function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
  e=o.createElement(i);r=o.getElementsByTagName(i)[0];
  e.src='//www.google-analytics.com/analytics.js';
  r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
  ga('create','<?php echo GOOGLE_ANALYTICS_ID; ?>');ga('send','pageview');
</script>
<?php
}

if (GOOGLE_ANALYTICS_ID) {
  add_action('wp_footer', 'ct_google_analytics', 20);
}

/*-------------------------------------------------------------------------------
  Pagination
-------------------------------------------------------------------------------*/

function pagination($pages = '', $range = 4) {
  $showitems = ($range * 2) + 1;

  global $paged;
  if (empty($paged)) $paged = 1;

  if ($pages == '') {
    global $wp_query;
    $pages = $wp_query->max_num_pages;
    if (!$pages) {
      $pages = 1;
    }
  }

    previous_posts_link(__('&laquo; Previous Page ', 'cinnamontoast'), $pages);

  if ($pages != 1) {
    if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
    if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";

    for ($i=1; $i <= $pages; $i++) {
      if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )) {
        echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
      }
    }

    if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\">Next &rsaquo;</a>";
    if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
  }

  next_posts_link(__('Next Page &raquo;', 'cinnamontoast'), $pages);
}

/*-------------------------------------------------------------------------------
  Admin Login Logo
-------------------------------------------------------------------------------*/

function new_login_logo() {
  $dimensions = unserialize(LOGIN_LOGO_SIZE); ?>
  <style type="text/css">
    <?php if ($dimensions[0] > 320) : ?>
    #login {
      width: <?php echo $dimensions[0] . 'px'; ?>;
    }
    <?php endif; ?>
    .login h1 a {
      background-image: url('<?php echo LOGIN_LOGO; ?>') !important;
      background-size: <?php echo $dimensions[0] . 'px ' . $dimensions[1] . 'px'; ?> !important;
      width: <?php echo $dimensions[0] . 'px'; ?> !important;
      height: <?php echo $dimensions[1] . 'px'; ?> !important;
    }
  </style>
<?php
}

if (LOGIN_LOGO && LOGIN_LOGO_SIZE) {
  add_action( 'login_head', 'new_login_logo' );
}

function wpc_url_login(){
  return get_bloginfo('url');
}

add_filter('login_headerurl', 'wpc_url_login');

function hide_login_nav() { ?>
  <style>
    #nav {
      display:none;
    }
  </style>
<?php
}

add_action( 'login_head', 'hide_login_nav' );

/*-------------------------------------------------------------------------------
  Gravity Forms - Advanced Custom Field Integration
-------------------------------------------------------------------------------*/

function gravity_forms_acf() {
  if (is_plugin_active('gravityforms/gravityforms.php') && is_plugin_active('advanced-custom-fields-pro/acf.php')) {
    class acf_field_gravity_forms extends acf_field {

      function __construct() {
        $this->name = 'gravity_forms_field';
        $this->label = __('Gravity Forms');
        $this->category = __("Relational",'acf');
        $this->defaults = array(
          'allow_multiple' => 0,
          'allow_null' => 0
        );
        parent::__construct();
      }

      function render_field_settings( $field ) {
        acf_render_field_setting( $field, array(
          'label' => 'Allow Null?',
          'type'  =>  'radio',
          'name'  =>  'allow_null',
          'choices' =>  array(
            1 =>  __("Yes",'acf'),
            0 =>  __("No",'acf'),
          ),
          'layout'  =>  'horizontal'
        ));

        acf_render_field_setting( $field, array(
          'label' => 'Allow Multiple?',
          'type'  =>  'radio',
          'name'  =>  'allow_multiple',
          'choices' =>  array(
            1 =>  __("Yes",'acf'),
            0 =>  __("No",'acf'),
          ),
          'layout'  =>  'horizontal'
        ));
      }

      function render_field( $field ) {
        $field = array_merge($this->defaults, $field);
        $choices = array();
        if (class_exists('RGFormsModel')) {
          $forms = RGFormsModel::get_forms(1);
        } else {
          echo "<font style='color:red;font-weight:bold;'>Warning: Gravity Forms is not installed or activated. This field does not function without Gravity Forms!</font>";
        }
        if (isset($forms)) {
          foreach( $forms as $form ) {
            $choices[ intval($form->id) ] = ucfirst($form->title);
          }
        }
        $field['choices'] = $choices;
        $field['type'] = 'select';
        if ( $field['allow_multiple'] ) {
          $multiple = 'multiple="multiple" data-multiple="1"';
          echo "<input type=\"hidden\" name=\"{$field['name']}\">";
        }
        else $multiple = '';
        ?>
        <select id="<?php echo str_replace(array('[',']'), array('-',''), $field['name']);?>" name="<?php echo $field['name']; if( $field['allow_multiple'] ) echo "[]"; ?>"<?php echo $multiple; ?>>
        <?php
        if ( $field['allow_null'] )
          echo '<option value="">- Select -</option>';
          foreach ($field['choices'] as $key => $value) :
            $selected = '';
            if ( (is_array($field['value']) && in_array($key, $field['value'])) || $field['value'] == $key )
              $selected = ' selected="selected"'; ?>
              <option value="<?php echo $key; ?>"<?php echo $selected;?>><?php echo $value; ?></option>
          <?php
          endforeach; ?>
        </select>
        <?php
      }

      function format_value( $value, $post_id, $field ) {
        if ( !$value || empty($value) ) {
          return false;
        }
        if ( is_array($value) && !empty($value) ) {
          $form_objects = array();
          foreach ($value as $k => $v) {
            $form = GFAPI::get_form( $v );
            if ( !is_wp_error($form) ) {
              $form_objects[$k] = $form;
            }
          }
          if ( !empty($form_objects) ) {
            return $form_objects;
          } else {
            return false;
          }
        } else {
          $form = GFAPI::get_form(intval($value));
          if ( !is_wp_error($form) ) {
            return $form;
          } else {
            return false;
          }
        }
      }
    }

    new acf_field_gravity_forms();
  }
}

add_action( 'admin_init', 'gravity_forms_acf' );

add_filter('acf_the_content', 'add_classes_to_heading');
add_filter('the_content', 'add_classes_to_heading');

function add_classes_to_heading($content){



  $headings = [
    'h1' => 'heading--primary',
    'h2' => 'heading--secondary',
    'h3' => 'heading--tertiary',
    'h4' => 'heading--quaternary',
    'h5' => 'heading--quinary',
    'h6' => 'heading--senary',

  ];

  libxml_clear_errors();

# use internal errors, don't spill out warnings
$previous = libxml_use_internal_errors(true);

  $dom = new DOMDocument();
  $dom->loadHTML('<wbr>' . $content . '</wbr>', LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD | LIBXML_NOWARNING );

  # clear errors list if any
libxml_clear_errors();

# restore previous behavior
libxml_use_internal_errors($previous);

  $xpath = new DOMXPath($dom);
  foreach($headings as $element => $class){
    $tags = $xpath->evaluate("//" . $element);
    // Loop through all the found tags
    foreach ($tags as $tag) {
        // Set class attribute
        $old_class = $tag->getAttribute("class");

        $tag->setAttribute("class", $old_class . ' ' . $class);
    }
    // Save the HTML changes


  }
  $content = str_replace(['<p><wbr></p>','<p></wbr></p>','<wbr>','</wbr>'],'',$dom->saveHTML());
  return $content;
}

// Check if user is an admin
function is_admin_user() {
  $current_user = wp_get_current_user();
  if (user_can( $current_user, 'administrator' )) {
    // user is an admin
    return true;
  } else {
    return false;
  }
}

?>
