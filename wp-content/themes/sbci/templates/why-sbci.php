<?php get_template_part('templates/page_banner'); ?>
<?php $section_count = 0; ?>

<!-- Begin Why SBCI -->

<?php if (have_rows('page_modules')) :

while (have_rows('page_modules')) : the_row();

	// Case: Callout Module
	if (get_row_layout() === 'why_sbci_callout_module') {
		$section_count = $section_count + 1;
		$clone_data = get_sub_field('why_sbci_callout_module');

		$content = $clone_data['content'] ? $clone_data['content'] : '';
		$sub_content = $clone_data['sub_content'] ? $clone_data['sub_content'] : '';
		$image_one = $clone_data['image_one'] ? $clone_data['image_one'] : '';
		$image_two = $clone_data['image_two'] ? $clone_data['image_two'] : '';
		

		get_template_part('templates/template-parts/template_why-sbci/why_sbci_callout', '', array(
			'section_count' => $section_count,
			'content' => $content,
			'sub_content' => $sub_content,
			'image_one' => $image_one,
			'image_two' => $image_two,
		));



	// Case: Benefits Module
	} else if (get_row_layout() === 'why_sbci_benefits_module') {
		$section_count = $section_count + 1;
		$clone_data = get_sub_field('why_sbci_benefits_module');

		$title = $clone_data['title'] ? $clone_data['title'] : '';
		$benefits = $clone_data['benefits'] ? $clone_data['benefits'] : '';
		

		get_template_part('templates/template-parts/template_why-sbci/why_sbci_benefits', '', array(
			'section_count' => $section_count,
			'content' => $content,
			'title' => $title,
			'benefits' => $benefits,
		));

	// Case: Membership Module
	} else if (get_row_layout() === 'why_sbci_membership_module') {
		$section_count = $section_count + 1;
		$clone_data = get_sub_field('why_sbci_membership_module');

		$icon = $clone_data['icon'] ? $clone_data['icon'] : '';
		$content = $clone_data['content'] ? $clone_data['content'] : '';
		$link = $clone_data['link'] ? $clone_data['link'] : '';
		$pull_quote = $clone_data['pull_quote'] ? $clone_data['pull_quote'] : '';


		get_template_part('templates/template-parts/template_why-sbci/why_sbci_membership', '', array(
			'section_count' => $section_count,
			'icon' => $icon,
			'content' => $content,
			'link' => $link,
			'pull_quote' => $pull_quote,
		));

	} 

endwhile;
endif; ?>