<?php 
	$page_banner = get_field('background_image');
	$page_title = get_field('page_title');
	$add_breadcrumbs = get_field('add_breadcrumbs');
	$section_count = 0;
?>

<header class="page_banner">
	<div class="wrapper-banner">
		
		<?php if ($page_banner) { ?> <!-- If user chooses a banner bg, display it -->
			<img src="<?php echo $page_banner['url']; ?>">
		<?php } else { ?> <!-- Otherwise, show default -->
			<img src="/wp-content/themes/sbci/assets/img/image--page-banner.png">
        <?php } ?>
        
		<div class="title-breadcrumbs-container">
		    <div class="wrapper">
                <h1 class="page_title font__primary--50">
                    <?php esc_html_e( get_the_title(), 'cinnamontoast' ); ?>
                </h1>

                <?php if ($add_breadcrumbs == 1) { ?>
                    <div class="breadcrumbs">
                        <ul>
                            <?php if( have_rows('breadcrumbs') ):
                                while( have_rows('breadcrumbs') ) : the_row();
                                    $link = get_sub_field('link');
                                        $link_url = $link['url'];
                                        $link_title = $link['title'];
                                        $link_target = $link['target'] ? $link['target'] : '_self';?>

                                        <li><a href="<?php echo $link_url; ?>" target="<?php echo $link_target; ?>"><?php echo $link_title; ?></a></li>

                                <?php // End loop.
                                endwhile;
                            // No value.
                            else :
                                // Do something...
                            endif; ?>
                            <li><a href="<?php esc_html_e( get_the_permalink()); ?>"><?php esc_html_e( get_the_title()); ?></a></li>
                        </ul>
                    </div>
                <?php } ?>

            </div>
        </div>
	</div>
</header>