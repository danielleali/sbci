<footer class="section__footer">

	<div class="footer-top">
		<a class="brand" href="<?php echo home_url('/') ?>">
			<img src="/wp-content/themes/sbci/assets/img/image--logo-white.png">
		</a>

		<nav class="footer-menu">
			<a class="linkedin_icon" href="">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M4.98 3.5c0 1.381-1.11 2.5-2.48 2.5s-2.48-1.119-2.48-2.5c0-1.38 1.11-2.5 2.48-2.5s2.48 1.12 2.48 2.5zm.02 4.5h-5v16h5v-16zm7.982 0h-4.968v16h4.969v-8.399c0-4.67 6.029-5.052 6.029 0v8.399h4.988v-10.131c0-7.88-8.922-7.593-11.018-3.714v-2.155z"/></svg>
			</a>
	    	<?php
	        if (has_nav_menu('footer_navigation')) :
	          wp_nav_menu(array('theme_location' => 'footer_navigation', 'menu_class' => 'nav navbar-nav'));
	        endif;
	      	?>
	    </nav>
	</div>

	<div class="footer-bottom">
		<p class="date">&copy; <?php echo date('Y'); ?></p>
			<nav class="footer-sub-menu">
				<?php
				if (has_nav_menu('footer_sub_navigation')) :
				wp_nav_menu(array('theme_location' => 'footer_sub_navigation', 'menu_class' => 'nav navbar-nav'));
				endif;
				?>
			</nav>
	</div>

	
	<a href="#header" class="scroll-top">
		<!-- <div class="scrolltop_btn"><div class="icon"></div></div> -->
		<div class="scrolltop_btn"><span></span></div>
		<!-- <div class="right-arrow"><span></span></div> -->
	</a>


</footer>

<?php wp_footer(); ?>