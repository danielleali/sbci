<!-- Global Variables -->
<?php $section_count = 0; ?>

<!-- Get Page Banner Template Part -->
<?php get_template_part('templates/page_banner'); ?>


<!-- Begin Services -->
<?php if (have_rows('page_modules')) :
	while (have_rows('page_modules')) : the_row();

		// Case: Callout Module
		if (get_row_layout() === 'services_template_module') {
			$section_count = $section_count + 1;
			$clone_data = get_sub_field('services_template_module');
			$links_title = $clone_data['links_title'] ? $clone_data['links_title'] : '';
			$links = $clone_data['links'] ? $clone_data['links'] : '';
			$main_content = $clone_data['main_content'] ? $clone_data['main_content'] : '';
			$sub_content = $clone_data['sub_content'] ? $clone_data['sub_content'] : '';
			$title = $clone_data['title'] ? $clone_data['title'] : '';
			$accordions = $clone_data['accordions'] ? $clone_data['accordions'] : '';
			
			get_template_part('templates/template-parts/template_services/services-template', '', array(
				'section_count' => $section_count,
				'links_title' => $links_title,
				'links' => $links,
				'main_content' => $main_content,
				'sub_content' => $sub_content,
				'title' => $title,
				'accordions' => $accordions,
			));


		} else if (get_row_layout() === 'services_callout_module') {
			$section_count = $section_count + 1;
			$clone_data = get_sub_field('services_callout_module');
			$background_image = $clone_data['background_image'] ? $clone_data['background_image'] : '';
			$title = $clone_data['title'] ? $clone_data['title'] : '';
			$callout_title = $clone_data['callout_title'] ? $clone_data['callout_title'] : '';
			$service_benefits = $clone_data['service_benefits'] ? $clone_data['service_benefits'] : '';

			get_template_part('templates/template-parts/template_services/services-callout', '', array(
				'section_count' => $section_count,
				'background_image' => $background_image,
				'title' => $title,
				'callout_title' => $callout_title,
				'service_benefits' => $service_benefits,
			));


		} else if (get_row_layout() === 'services_become_a_member_module') {
			$section_count = $section_count + 1;
			$clone_data = get_sub_field('services_become_a_member_module');
			$title = $clone_data['title'] ? $clone_data['title'] : '';
			$content = $clone_data['content'] ? $clone_data['content'] : '';
			$get_started_link = $clone_data['get_started_link'] ? $clone_data['get_started_link'] : '';
			$member_box_title = $clone_data['member_box_title'] ? $clone_data['member_box_title'] : '';
			$member_box_content = $clone_data['member_box_content'] ? $clone_data['member_box_content'] : '';

			get_template_part('templates/template-parts/template_services/services-member', '', array(
				'section_count' => $section_count,
				'title' => $title,
				'content' => $content,
				'get_started_link' => $get_started_link,
				'member_box_title' => $member_box_title,
				'member_box_content' => $member_box_content,
			));
		}

	endwhile;
endif; ?>