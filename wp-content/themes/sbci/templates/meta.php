<?php 
global $post;

if (is_home()) {
	$page_id = get_option( 'page_for_posts' );
} else if (is_front_page()) {
	$page_id = get_option(' page_on_front' );
} else {
	if (have_posts()) {
		while (have_posts()) {
			the_post();
			$page_id = $post->ID;
		}
	}
}

if ($page_id == 0) $page_id = 'option';

$metaTitle = get_field('meta_title', $page_id);
if ($metaTitle == null) {
	$title = wp_title('|', false, 'right');
} else {
	$title = $metaTitle;
}

$metaDescription = get_field('meta_description', $page_id);
if ($metaDescription == null) {
	$description = get_bloginfo('description');
} else {
	$description = $metaDescription;
}

if ($page_id == 0 || $page_id == null) {
	$url = get_bloginfo('url');
} else {
	$url = get_permalink($page_id);
}
$twitter = get_field('twitter', 'option'); 
?>

	<meta property="og:type" content="website">
	<meta property="og:title" content="<?php echo $title; ?>">
	<meta property="og:url" content="<?php echo $url; ?>">
	<meta property="og:description" content="<?php echo $description; ?>">
	<meta property="description" content="<?php echo $description; ?>">

<?php if (!empty($twitter)) : ?>
	<meta name="twitter:site" content="@<?php echo $twitter; ?>">
	<meta name="twitter:creator" content="@<?php echo $twitter; ?>">
<?php endif; ?>
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:title" content="<?php echo $title; ?>">
	<meta name="twitter:url" content="<?php echo $url; ?>">
	<meta name="twitter:description" content="<?php echo $description; ?>">

<?php 
$featured_image = wp_get_attachment_image_src(get_post_thumbnail_id($page_id), 'full'); 
$default_share_image = get_field('default_share_image', 'option');
if ($featured_image[0] != null) : ?>
	<meta property="og:image" content="<?php echo $featured_image[0] ?>">
	<meta name="twitter:image:src" content="<?php echo $featured_image[0]; ?>">
<?php else : ?>
	<meta property="og:image" content="<?php echo $default_share_image; ?>">
	<meta name="twitter:image:src" content="<?php echo $default_share_image; ?>">
<?php endif; ?>

	<title><?php echo $title; ?></title>

