<svg 
 xmlns="http://www.w3.org/2000/svg"
 xmlns:xlink="http://www.w3.org/1999/xlink"
 width="20px" height="19px">
<path fill-rule="evenodd"  fill="rgb(160, 207, 109)"
 d="M0.781,0.281 L14.375,0.281 C17.136,0.281 19.375,2.520 19.375,5.281 L19.375,18.844 L5.781,18.844 C3.020,18.844 0.781,16.605 0.781,13.844 L0.781,0.281 Z"/>
</svg>