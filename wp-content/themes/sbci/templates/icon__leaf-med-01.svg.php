<svg 
 xmlns="http://www.w3.org/2000/svg"
 xmlns:xlink="http://www.w3.org/1999/xlink"
 width="51px" height="51px">
<path fill-rule="evenodd"  fill="rgb(16, 169, 149)"
 d="M15.781,0.250 L50.969,0.250 L50.969,35.500 C50.969,43.784 44.253,50.500 35.969,50.500 L0.781,50.500 L0.781,15.250 C0.781,6.966 7.497,0.250 15.781,0.250 Z"/>
</svg>