<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 27.03 26.96">
<defs>
    <style>.cls-1{fill:none;stroke:#042c33;stroke-miterlimit:10;stroke-width:4px;}</style>
</defs>
<g id="Layer_2" data-name="Layer 2">
    <g id="Layer_1-2" data-name="Layer 1">
        <polyline class="cls-1 one" points="13.52 0 13.52 13.52 0 13.52"/>
        <polyline class="cls-1 two" points="13.52 26.96 13.52 13.45 27.03 13.45"/>
    </g>
</g>
</svg>