<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 16.47 21.85">
<defs>
    <style>.cls-1,.cls-3{fill:none;}.cls-2{clip-path:url(#clip-path);}.cls-3{stroke:#231f20;stroke-miterlimit:10;stroke-width:2px;}.cls-4{fill:#231f20;}</style>
    <clipPath id="clip-path" transform="translate(0 0)"><rect class="cls-1" width="16.47" height="21.85"/></clipPath>
</defs>
<title>icon__phone</title>
<g class="cls-2">
    <rect class="cls-3" x="1" y="1" width="14.47" height="19.85" rx="1.79"/>
    <path class="cls-4" d="M4.73,3.62h7a.76.76,0,0,0,.76-.76V1.38H4V2.86a.76.76,0,0,0,.76.76" transform="translate(0 0)"/>
    <line class="cls-3" x1="6.29" y1="17.05" x2="10.18" y2="17.05"/>
</g>
</svg>