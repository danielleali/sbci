<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 18.47 18.22">
<defs>
    <style>.cls-1,.cls-3{fill:none;}.cls-2{clip-path:url(#clip-path);}.cls-3{stroke:#231f20;stroke-miterlimit:10;stroke-width:2px;}</style>
    <clipPath id="clip-path" transform="translate(0 0)"><rect class="cls-1" width="18.47" height="18.22"/></clipPath>
</defs>
<title>icon__fax</title>
<g class="cls-2">
    <path class="cls-3" d="M4.28,13.68H1.73A.73.73,0,0,1,1,13V7.32a.73.73,0,0,1,.73-.72h15a.73.73,0,0,1,.73.72V13a.73.73,0,0,1-.73.73H14.19" transform="translate(0 0)"/>
    <rect class="cls-3" x="4.28" y="1" width="9.91" height="5.6"/>
    <rect class="cls-3" x="4.28" y="10.14" width="9.91" height="7.08"/>
    <line class="cls-3" x1="7.22" y1="13.68" x2="11.25" y2="13.68"/>
</g>
</svg>