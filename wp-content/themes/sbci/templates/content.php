<?php 
	$page_banner = get_field('background_image');
	$page_title = get_field('page_title');
	$add_breadcrumbs = get_field('add_breadcrumbs');
	$section_count = 0;
?>

<header class="page_banner">
	<div class="wrapper-banner">
		
		<?php if ($page_banner) { ?> <!-- If user chooses a banner bg, display it -->
			<img src="<?php echo $page_banner['url']; ?>">
		<?php } else { ?> <!-- Otherwise, show default -->
			<img src="/wp-content/themes/sbci/assets/img/image--page-banner.png">
		<?php } ?>

		<div class="title-breadcrumbs-container">
		
		<h1 class="page_title font__primary--50">
			<?php esc_html_e( get_the_title(), 'cinnamontoast' ); ?>
		</h1>

		<?php if ($add_breadcrumbs == 1) { ?>
			<div class="breadcrumbs">
				<ul>
					<?php if( have_rows('breadcrumbs') ):
						while( have_rows('breadcrumbs') ) : the_row();
							$link = get_sub_field('link');
								$link_url = $link['url'];
								$link_title = $link['title'];
								$link_target = $link['target'] ? $link['target'] : '_self';?>

								<li><a href="<?php echo $link_url; ?>" target="<?php echo $link_target; ?>"><?php echo $link_title; ?></a></li>

						<?php // End loop.
						endwhile;
					// No value.
					else :
						// Do something...
					endif; ?>
					<li><a href="<?php esc_html_e( get_the_permalink()); ?>"><?php esc_html_e( get_the_title()); ?></a></li>
				</ul>
			</div>
		<?php } ?>

		</div>

	</div>
</header>

<!-- Begin Pattern Library -->

<?php if (have_rows('page_modules')) :

	while (have_rows('page_modules')) : the_row();

		// Case: 
		if (get_row_layout() === 'main_content_module') {
			$section_count = $section_count + 1;
			$clone_data = get_sub_field('main_content_module');
			$layout = $clone_data['layout'] ? $clone_data['layout'] : 'two-col';
			$section_background_color = $clone_data['section_background_color'] ? $clone_data['section_background_color'] : '';
			// Video Specific
			$section_title_videos = $clone_data['section_title_videos'] ? $clone_data['section_title_videos'] : '';
			$add_content_beside_video = $clone_data['add_content_beside_video'] ? $clone_data['add_content_beside_video'] : '';
			$accompanying_title = $clone_data['accompanying_title'] ? $clone_data['accompanying_title'] : '';
			$accompanying_description = $clone_data['accompanying_description'] ? $clone_data['accompanying_description'] : '';
 
			// Titles
			$column_one_title = $clone_data['column_one_title'] ? $clone_data['column_one_title'] : '';
			$column_two_title = $clone_data['column_2_title'] ? $clone_data['column_2_title'] : '';
			$column_three_title = $clone_data['column_3_title'] ? $clone_data['column_3_title'] : '';

			// Column Type
			$column_one_type = $clone_data['column_one_type'] ? $clone_data['column_one_type'] : '';
			$column_two_type = $clone_data['column_two_type'] ? $clone_data['column_two_type'] : '';
			$column_three_type = $clone_data['column_three_type'] ? $clone_data['column_three_type'] : '';

			// Column Content
			$column_one_content = $clone_data['column_1_content'] ? $clone_data['column_1_content'] : '';
			$column_two_content = $clone_data['column_2_content'] ? $clone_data['column_2_content'] : '';
			$column_three_content = $clone_data['column_3_content'] ? $clone_data['column_3_content'] : '';

			// Column Images
			$column_one_image = $clone_data['column_one_image'] ? $clone_data['column_one_image'] : '';
			$column_two_image = $clone_data['column_2_image'] ? $clone_data['column_2_image'] : '';
			$column_three_image = $clone_data['column_three_image'] ? $clone_data['column_three_image'] : '';

			$image_one_caption = $clone_data['image_1_caption'] ? $clone_data['image_1_caption'] : '';
			$image_two_caption = $clone_data['image_2_caption'] ? $clone_data['image_2_caption'] : '';
			$image_three_caption = $clone_data['image_three_caption'] ? $clone_data['image_three_caption'] : '';

			// Column Quotes - New?
			$column_one_quote = $clone_data['column_1_quote'] ? $clone_data['column_1_quote'] : '';
			$column_two_quote = $clone_data['column_2_quote'] ? $clone_data['column_2_quote'] : '';

			// Column Quotes - not used?
			$column_one_pq_repeater = $clone_data['column_one_pull_quote'] ? $clone_data['column_one_pull_quote'] : '';
			$column_one_pq_quote = $clone_data['quote'] ? $clone_data['quote'] : '';
			$column_one_pq_author = $clone_data['author'] ? $clone_data['author'] : '';
			$column_one_pq_title = $clone_data['title'] ? $clone_data['title'] : '';

			// Column Videos
			$column_one_video = $clone_data['column_one_video'] ? $clone_data['column_one_video'] : '';
			$column_one_video_overlay = $clone_data['column_one_video_overlay'] ? $clone_data['column_one_video_overlay'] : '';
			$column_one_video_title = $clone_data['column_one_video_title'] ? $clone_data['column_one_video_title'] : '';
			$column_one_video_description = $clone_data['column_one_video_description'] ? $clone_data['column_one_video_description'] : '';
			$column_two_video = $clone_data['column_two_video'] ? $clone_data['column_two_video'] : '';
			$column_two_video_overlay = $clone_data['column_two_video_overlay'] ? $clone_data['column_two_video_overlay'] : '';
			$column_two_video_title = $clone_data['column_two_video_title'] ? $clone_data['column_two_video_title'] : '';
			$column_two_video_description = $clone_data['column_two_video_description'] ? $clone_data['column_two_video_description'] : '';

			// Column Lists - not used?
			$column_one_list_title = $clone_data['column_one_list_title'] ? $clone_data['column_one_list_title'] : '';
			$column_one_list = $clone_data['column_one_list'] ? $clone_data['column_one_list'] : '';
			$column_two_list_title = $clone_data['column_two_list_title'] ? $clone_data['column_two_list_title'] : '';
			$column_two_list = $clone_data['column_two_list'] ? $clone_data['column_two_list'] : '';
			$column_three_list_title = $clone_data['column_three_list_title'] ? $clone_data['column_three_list_title'] : '';
			$column_three_list = $clone_data['column_three_list'] ? $clone_data['column_three_list'] : '';

			

			get_template_part('templates/template-parts/main-content', '', array(
				'layout' => $layout,
				'section_background_color' => $section_background_color,
				'section_count' => $section_count,
				'section_title_videos' => $section_title_videos,
				'add_content_beside_video' => $add_content_beside_video,
				'accompanying_title' => $accompanying_title,
				'accompanying_description' => $accompanying_description,

				'column_one_type' => $column_one_type,
				'column_two_type' => $column_two_type,
				'column_three_type' => $column_three_type,

				'column_one_title' => $column_one_title,
				'column_two_title' => $column_two_title,
				'column_three_title' => $column_three_title,

				'column_one_content' => $column_one_content,
				'column_two_content' => $column_two_content,
				'column_three_content' => $column_three_content,

				'column_one_image' => $column_one_image,
				'column_two_image' => $column_two_image,
				'column_three_image' => $column_three_image,

				'image_one_caption' => $image_one_caption,
				'image_two_caption' => $image_two_caption,
				'image_three_caption' => $image_three_caption,

				'column_one_quote' => $column_one_quote,
				'column_two_quote' => $column_two_quote,

				'column_one_video' => $column_one_video,
				'column_one_video_overlay' => $column_one_video_overlay,
				'column_one_video_title' => $column_one_video_title,
				'column_one_video_description' => $column_one_video_description,
				'column_two_video' => $column_two_video,
				'column_two_video_overlay' => $column_two_video_overlay,
				'column_two_video_title' => $column_two_video_title,
				'column_two_video_description' => $column_two_video_description,

			));



		} else if (get_row_layout() === 'photo_gallery_module') {
			$section_count = $section_count + 1;
			$clone_data_1 = get_sub_field('photo_gallery_module');
			
			$type = $clone_data_1['type'] ? $clone_data_1['type'] : '';
			$title = $clone_data_1['title'] ? $clone_data_1['title'] : '';
			$gallery = $clone_data_1['gallery'] ? $clone_data_1['gallery'] : '';
			$grid = $clone_data_1['grid'] ? $clone_data_1['grid'] : '';

			get_template_part('templates/template-parts/image-gallery', '', array(
				'section_count' => $section_count,
				'type' => $type,
				'title' => $title,
				'gallery' => $gallery,
				'grid' => $grid,
			));



		} else if (get_row_layout() === 'accordion_module') {
			$section_count = $section_count + 1;
			$clone_data_1 = get_sub_field('accordion_module');
			
			$section_title = $clone_data_1['section_title'] ? $clone_data_1['section_title'] : '';
			$accordions = $clone_data_1['accordions'] ? $clone_data_1['accordions'] : '';


			get_template_part('templates/template-parts/accordion', '', array(
				'section_count' => $section_count,
				'section_title' => $section_title,
				'accordions' => $accordions,

			));



		} else if (get_row_layout() === 'table_module') {
			$section_count = $section_count + 1;
			$clone_data_1 = get_sub_field('table_module');
			$table = $clone_data_1['table'] ? $clone_data_1['table'] : '';

			get_template_part('templates/template-parts/table', '', array(
				'section_count' => $section_count,
				'table' => $table,
			));



		} else if (get_row_layout() === 'form_module') {
			$section_count = $section_count + 1;
			$clone_data_1 = get_sub_field('form_module');
			$form = $clone_data_1['form'] ? $clone_data_1['form'] : '';

			get_template_part('templates/template-parts/form', '', array(
				'section_count' => $section_count,
				'form' => $form,
			));




		} else if (get_row_layout() === 'logo_module') {
			$section_count = $section_count + 1;
			$clone_data_1 = get_sub_field('logo_module');
			
			$section_title = $clone_data_1['section_title'] ? $clone_data_1['section_title'] : '';
			$section_background_color = $clone_data_1['section_background_color'] ? $clone_data_1['section_background_color'] : '';
			$logos = $clone_data_1['logos'] ? $clone_data_1['logos'] : '';

			get_template_part('templates/template-parts/logos', '', array(
				'section_count' => $section_count,
				'section_title' => $section_title,
				'section_background_color' => $section_background_color,
				'logos' => $logos,
			));




		} else if (get_row_layout() === 'people_module') {
			$section_count = $section_count + 1;
			$clone_data_1 = get_sub_field('people_module');
			$section_title = $clone_data_1['section_title'] ? $clone_data_1['section_title'] : '';

			$board_of_directors = $clone_data_1['board_of_directors'] ? $clone_data_1['board_of_directors'] : '';
			$staff = $clone_data_1['staff'] ? $clone_data_1['staff'] : '';
			$consultants = $clone_data_1['consultants'] ? $clone_data_1['consultants'] : '';

			$bod_title = $clone_data_1['bod_title'] ? $clone_data_1['bod_title'] : '';
			$staff_title = $clone_data_1['staff_title'] ? $clone_data_1['staff_title'] : '';
			$consultants_title = $clone_data_1['consultants_title'] ? $clone_data_1['consultants_title'] : '';

			get_template_part('templates/template-parts/people', '', array(
				'section_count' => $section_count,
				'section_title' => $section_title,

				'board_of_directors' => $board_of_directors,
				'staff' => $staff,
				'consultants' => $consultants,
				
				'bod_title' => $bod_title,
				'staff_title' => $staff_title,
				'consultants_title' => $consultants_title,
			));





		} else if (get_row_layout() === 'contact_us_module') {
			$section_count = $section_count + 1;
			$clone_data_1 = get_sub_field('contact_us_module');
			$section_title = $clone_data_1['section_title'] ? $clone_data_1['section_title'] : '';

			$contact_title = $clone_data_1['contact_title'] ? $clone_data_1['contact_title'] : '';
			$contact_description = $clone_data_1['contact_description'] ? $clone_data_1['contact_description'] : '';
			$contact_location = $clone_data_1['contact_location'] ? $clone_data_1['contact_location'] : '';
			$contact_phone = $clone_data_1['contact_phone'] ? $clone_data_1['contact_phone'] : '';
			$contact_phone_toll_free = $clone_data_1['contact_phone_toll_free'] ? $clone_data_1['contact_phone_toll_free'] : '';
			$contact_fax = $clone_data_1['contact_fax'] ? $clone_data_1['contact_fax'] : '';
			$contact_fax_toll_free = $clone_data_1['contact_fax_toll_free'] ? $clone_data_1['contact_fax_toll_free'] : '';
			$contact_email = $clone_data_1['contact_email'] ? $clone_data_1['contact_email'] : '';

			$admin_title = $clone_data_1['admin_title'] ? $clone_data_1['admin_title'] : '';
			$administrators = $clone_data_1['administrators'] ? $clone_data_1['administrators'] : '';

			get_template_part('templates/template-parts/contact', '', array(
				'section_count' => $section_count,
				'section_title' => $section_title,

				'contact_title' => $contact_title,
				'contact_description' => $contact_description,
				'contact_location' => $contact_location,
				'contact_phone' => $contact_phone,
				'contact_phone_toll_free' => $contact_phone_toll_free,
				'contact_fax' => $contact_fax,
				'contact_fax_toll_free' => $contact_fax_toll_free,
				'contact_email' => $contact_email,

				'admin_title' => $admin_title,
				'administrators' => $administrators,
			));

		} 

	
	endwhile;
endif; ?>








