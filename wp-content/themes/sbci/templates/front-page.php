

<?php
	// Front Page
	if( have_rows('modules') ):
		while ( have_rows('modules') ) : the_row();

			// Case: Page Banner
			if( get_row_layout() == 'page_banner' ): 
				$title = get_sub_field('title');
				$intro = get_sub_field('intro_text');
				$image = get_sub_field('image'); ?>

			<section class="section section__page-banner">
				<div class="wrapper">
					<div class="content-wrapper">
						<div class="content">
							<div class="intro-text scrolling up">
								<?php if ($title) { ?>
									<p><?php echo $title; ?></p>
								<?php } ?>
							</div>
							<div class="paragraph-text scrolling left">
								<?php if ($intro) { ?>
									<p><?php echo $intro; ?></p>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
				<div class="image-wrapper">
					<img src="<?php echo $image['url']; ?>" alt="SBCI Homepage Banner">
				</div>
			</section>


			<?php
        	// Case: Featured Content
			elseif ( get_row_layout() == 'featured_content' ): ?>

				<section class="section section__featured-content"> 
					<div class="wrapper">
						<div class="wrapper-container">
							<div class="content-block_container scrolling right">
								<div class="decorative-line"></div>
								
								<?php
								// Repeater
								if( have_rows('content') ):
									while( have_rows('content') ) : the_row();
										$title = get_sub_field('title');
										$description = get_sub_field('description');
										$link = get_sub_field('link');
											$link_url = $link['url'];
											$link_title = $link['title'];
											$link_target = $link['target'] ? $link['target'] : '_self';
										$image = get_sub_field('image'); ?>

										<div class="content-left">
											<div class="title scrolling down">
												<?php if ($title) { ?><p><?php echo $title; ?></p><?php } ?>
											</div>
											<div class="desc scrolling down">
												<?php if ($description) { ?><p><?php echo $description; ?></p><?php } ?>
											</div>
											<a href="<?php echo ( $link_url ); ?>" class="button scrolling up" target="<?php echo esc_attr( $link_target ); ?>"><?php echo ( $link_title ); ?></a>
										</div>

										<div class="content-right">
											<div class="img-container">
												<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
											</div>
										</div>

									<?php
									endwhile;
								else :
								endif; ?>

							</div>
							
							<div class="toggle-pagination-container">
								<div class="toggles-container scrolling right">
		
									<div class="toggle-left">
										<!-- <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 11.06 17.89"><defs><style>.cls-1,.cls-3{fill:none;}.cls-2{clip-path:url(#clip-path);}.cls-3{;stroke-miterlimit:10;stroke-width:3px;}</style><clipPath id="clip-path" transform="translate(0 0)"><rect class="cls-1" width="11.06" height="17.89"/></clipPath></defs><title>left-arrow</title><g class="cls-2"><polyline class="cls-3" points="10 16.83 2.12 8.94 10 1.06"/></g></svg> -->
									</div>
									<div class="toggle-right">
										<div class="right-arrow"><span></span></div>
									</div>
								</div>
								<div class="pagination-container">
									<ol>
										<li class="item active scrolling down">1</li>
										<li class="item scrolling down">2</li>
										<li class="item scrolling down">3</li>
									</ol>
								</div>
							</div>
						</div>

					</div>
				</section>




			<?php
			// Case: Featured Content
			elseif ( get_row_layout() == 'services' ): 

				$section_title = get_sub_field('section_title');
				$cta_title = get_sub_field('cta_title');
				$cta_content = get_sub_field('cta_content');
				$cta_link = get_sub_field('cta_link');
					$clink_url = $cta_link['url'];
					$clink_title = $cta_link['title'];
					$clink_target = $cta_link['target'] ? $cta_link['target'] : '_self'; ?>

				<section class="section section__services">
					<div class="wrapper">
						<div class="section_title scrolling down">
							<h3><?php echo $section_title;?></h3>
						</div>
						<div class="service-cards__container">

							<?php 
							if( have_rows('cards') ):
								while( have_rows('cards') ) : the_row();

									$card_letter = get_sub_field('card_letter');
									$title = get_sub_field('title');
									$description = get_sub_field('description');
									$link = get_sub_field('link');
										$link_url = $link['url'];
										$link_title = $link['title'];
										$link_target = $link['target'] ? $link['target'] : '_self'; ?>

									<div class="card scrolling up">
										<div class="card-num"><?php echo $card_letter; ?></div>
										<div class="card-title"><?php echo $title; ?></div>
										<div class="card-content-container">
											<div class="card-desc scrolling right"><?php echo $description; ?></div>
											<div class="card-link scrolling right"><a href="<?php echo ( $link_url ); ?>" class="button scrolling up" target="<?php echo esc_attr( $link_target ); ?>"><?php echo ( $link_title ); ?></a></div>
										</div>
									</div>
								
								<?php
								endwhile;
							else :
							endif; ?>

						</div>


						<div class="cta_callout scrolling up">
							<div class="content_container">
								<div class="content-left">
									<div class="icon"></div>
									<div class="section_title scrolling down">
										<h3><?php echo $cta_title; ?></h3>
									</div>
								</div>
								<div class="content-right scrolling right">
									<p class=""><?php echo $cta_content; ?></p>
									<div class="btn btn__secondary"><a href="<?php echo ( $clink_url ); ?>" target="<?php echo esc_attr( $clink_target ); ?>"><?php echo ( $clink_title ); ?></a></div>
								</div>
							</div>
						</div>
					</div>
				</section>



			<?php
			// Case: Featured Content
			elseif ( get_row_layout() == 'resources' ): 
				$section_title = get_sub_field('section_title');
				$rlink = get_sub_field('link');
					$rlink_url = $rlink['url'];
					$rlink_title = $rlink['title'];
					$rlink_target = $rlink['target'] ? $rlink['target'] : '_self';
				$resources = get_sub_field('resources'); ?>

				<section class="section section__resources" style="background-image: url('http://sbci-new.ctclients.ca/wp-content/uploads/2020/11/image-grey-bg.png');">
					<div class="wrapper">
						<div class="section_top">
							<div class="section_title scrolling down"><?php echo $section_title; ?></div>
							<div class="btn btn__primary scrolling down"><a href="<?php echo $rlink_url; ?>" target="<?php echo $rlink_target; ?>"><?php echo $rlink_title; ?></a></div>
						</div>
						<div class="resource-cards__container">

							<?php
								if( have_rows('resources') ):
									while( have_rows('resources') ) : the_row();

										$resource = get_sub_field('resource');

                                    	$permalink = get_permalink($resource->ID);
										$type = esc_html($resource->type);
										$title = get_the_title($resource);
										$content = esc_html($resource->content);
										?>

										<div class="card scrolling up">
											<div class="card-type-container scrolling right">
												<div class="icon public"></div>
												<div class="text"><p><?php echo $type; ?> Resource</p></div>
											</div>
											<div class="card-content-container scrolling right">
												<div class="title"><?php echo $title; ?></div>
												<div class="desc"><?php echo $content; ?></div>
												
												<?php if ($type == 'public') { ?>
													<div class="card-link-container scrolling right">
														<a href="#">Read More</a>
													</div>
												<?php } else if ($type == 'member') { ?>
													<!-- 
														Eventually will have to test if user is logged in or not. 
														If not logged in, display Sign In and Become Member 
														If they are logged in, display a Read More button link
													-->
													<div class="card-link-container-mulitple scrolling right">
														<a href="#">Sign In</a>
														<a href="#">Become a Member</a>
													</div>
												<?php } ?>
											</div>
										</div>

									<?php
									endwhile;
								else :
								endif; ?>
						</div>
						<div class="container-bottom">
							<div class="btn btn__primary scrolling down"><a href="<?php echo $rlink_url; ?>" target="<?php echo $rlink_target; ?>"><?php echo $rlink_title; ?></a></div>
						</div>
					</div>
				</section>


			<?php
			// Case: Featured Content
			elseif ( get_row_layout() == 'news_and_events' ): 
				$section_title = get_sub_field('section_title');
				$nlink = get_sub_field('link');
					$nlink_url = $nlink['url'];
					$nlink_title = $nlink['title'];
					$nlink_target = $nlink['target'] ? $nlink['target'] : '_self'; ?>

			<section class="section section__news-events">
				<img src="http://sbci-new.ctclients.ca/wp-content/uploads/2020/10/Large-SBCI-Graphic.png" class="sbci_graphic">
				<!-- /wp-content/uploads/my-image.png -->
				<div class="wrapper">

					<div class="section_left">
						<div class="section_title scrolling down"><?php echo $section_title; ?></div>

						<!-- Show on Tablet/Desktop Only -->
						<div class="btn__primary scrolling up"><a href="<?php echo $nlink_url; ?>" target="<?php echo $nlink_target; ?>"><?php echo $nlink_title; ?></a></div>
					</div>



					<div class="newsevents-cards__container">

						<?php $posts = get_posts(array(
							'posts_per_page'	=> 3,
							'post_type'			=> 'news-events',
							'meta_key'          => 'date',
							'orderby'           => 'date',
							'order'             => 'ASC',
						)); ?>

						<?php if( $posts ) { ?>  
							<?php foreach( $posts as $post ): 
								$title = get_the_title();
								$type = get_field('type', $post->ID);
									if ($type == 'news') :
										$card_type = 'news';
									endif;
									if ($type == 'event') :
										$card_type = 'event';
									endif;
								$cover_image = get_field('cover_image', $post->ID);
								$date = get_field('date', $post->ID);
								$content = get_field('content', $post->ID);
								?>

					
						<?php if (!$cover_image) : ?>
							<div class="card no-img scrolling right">
								<div class="card-top-overlay"></div>
								<div class="card-top"></div>
							
						<?php else : ?>
							<div class="card has-img scrolling right">
								<div class="card-top-overlay"></div>
								<div class="card-top-img" style="background-image: url('<?php echo $cover_image; ?>');">
							</div>
						<?php endif; ?>

								<div class="card-type-container scrolling left" >
									<?php if ($type == 'news') : ?>
										<div class="icon">
											<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 27.45 25.74"><defs><style>.cls-1,.cls-2{fill:none;}.cls-2{stroke:#fff;stroke-miterlimit:10;stroke-width:2px;}.cls-3{clip-path:url(#clip-path);}</style><clipPath id="clip-path"><rect class="cls-1" width="27.47" height="25.74"/></clipPath></defs><title>icon--news</title><line class="cls-2" x1="12.77" y1="6.83" x2="15.32" y2="6.83"/><line class="cls-2" x1="12.77" y1="10.83" x2="15.32" y2="10.83"/><line class="cls-2" x1="4.89" y1="14.91" x2="15.32" y2="14.91"/><line class="cls-2" x1="4.89" y1="18.91" x2="15.32" y2="18.91"/><g class="cls-3"><path class="cls-2" d="M23.25,24.74h0A3.24,3.24,0,0,1,20,21.53h0V4.7h6.45V21.52A3.22,3.22,0,0,1,23.25,24.74Z"/><path class="cls-2" d="M20,11V1H1V21.61a3.13,3.13,0,0,0,3.13,3.13H23.25"/><rect class="cls-2" x="5.86" y="6.83" width="4.59" height="4"/></g></svg>
										</div>
									<?php elseif ($type == 'event') : ?>
										<div class="icon"></div>	
									<?php endif; ?>

									<?php if (!$cover_image) : ?>
										<div class="text"><p><?php echo $type; ?></p></div>
									<?php else : ?>
										<div class="text has-image"><p><?php echo $type; ?></p></div>
									<?php endif; ?>
									
								</div>

								<div class="card-content-container scrolling down">
									<div class="title"><?php echo $title; ?></div>
									<div class="date"><?php echo $date; ?></div>
									<div class="desc"><?php echo $content; ?></div>
									<div class="card-link-container scrolling up">
										<a href="#">Read More</a>
									</div>
								</div>
						
							</div><!-- card -->

						<?php 
							endforeach; 
							wp_reset_postdata();
						?>
							<?php } ?>
					
					</div><!-- newsevents-cards__container -->

					<div class="container-bottom">
						<div class="btn__primary scrolling up"><a href="<?php echo $nlink_url; ?>" target="<?php echo $nlink_target; ?>"><?php echo $nlink_title; ?></a></div>
					</div>
					

				</div><!-- wrapper -->
				
				
			</section><!-- section -->
			
			<?php
			// Case: Member Block
			elseif ( get_row_layout() == 'member' ): 
				$title = get_sub_field('member_title');
				$content = get_sub_field('member_content');
				$mlink = get_sub_field('member_link');
					$mlink_url = $mlink['url'];
					$mlink_title = $mlink['title'];
					$mlink_target = $mlink['target'] ? $mlink['target'] : '_self'; ?>


				<section class="section section__member">
					<div class="wrapper">
						<div class="background-block"></div>
						<div class="membership-block-container scrolling up" style="background-image:url('http://sbci-new.ctclients.ca/wp-content/uploads/2020/10/green-bg.png');">
							<div class="content_container">
								<div class="section_left  scrolling right">
									<div class="icon"></div>
									<div class="text"><?php echo $title; ?></div>
								</div>
								<div class="section_right  scrolling left">
									<div class="text"><?php echo $content; ?></div>
									<div class="btn__secondary">
										<a href="<?php echo $mlink_url; ?>" target="<?php echo $mlink_target; ?>"><?php echo $mlink_title; ?></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>


			<?php
			else :
			endif;
		// End modules loop.
		endwhile;
	endif; ?>





