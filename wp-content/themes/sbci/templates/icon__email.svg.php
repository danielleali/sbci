<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 18.47 21.14">
<defs>
    <style>.cls-1,.cls-3{fill:none;}.cls-2{clip-path:url(#clip-path);}.cls-3{stroke:#231f20;stroke-miterlimit:10;stroke-width:2px;}</style>
    <clipPath id="clip-path" transform="translate(0 0)">
        <rect class="cls-1" width="18.47" height="21.14"/></clipPath>
    </defs><title>icon__email</title>
    <g class="cls-2">
        <path class="cls-3" d="M2.61,1A1.61,1.61,0,0,0,1,2.61v13.3H6V19l3.71-3.1h6.14a1.61,1.61,0,0,0,1.61-1.61V1Z" transform="translate(0 0)"/>
        <line class="cls-3" x1="5.24" y1="6.55" x2="13.24" y2="6.55"/><line class="cls-3" x1="5.24" y1="10.55" x2="13.24" y2="10.55"/>
    </g>
</svg>