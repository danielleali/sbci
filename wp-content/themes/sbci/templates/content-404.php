<article <?php post_class(); ?>>
	<header>
		<h2 class="entry-title">
			<a href="<?php bloginfo('wpurl'); ?>"><?php _e('We\'re sorry, page not found', 'cinnamontoast'); ?></a>
		</h2>
	</header>
	<section class="content">
		<?php _e('The page you were looking for could not be found. Please try again.', 'cinnamontoast'); ?>
	</section>
</article>