<a href="#main" class="skip-to-content">Skip to Content</a>

<header class="section__header" id="header">


<div class="nav-desktop">

    <div class="nav-left">
       <!-- brand -->
      <a class="brand" href="<?php echo home_url('/') ?>">
        <img src="http://sbci-new.ctclients.ca/wp-content/uploads/2020/10/image-logo.png">
      </a>
    </div>


    <div class="nav-right">
      <div class="nav-top">
        <!-- search, employee portal, membership -->
        <svg id="search-icon" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 21 27.75"><defs><style>.cls-1,.cls-3{fill:none;}.cls-2{clip-path:url(#clip-path);}.cls-3{stroke-miterlimit:10;stroke-width:3px;}</style><clipPath id="clip-path"><rect class="cls-1" width="21" height="27.75"/></clipPath></defs><title>search-icon</title><g class="cls-2"><circle class="cls-3" cx="10.5" cy="10.5" r="9"/><line class="cls-3" x1="10.75" y1="19.75" x2="10.75" y2="27.75"/></g></svg>
        
        <div class="search_expanded">
          <form action="/action_page.php">
            <input type="text" id="search" name="search" placeholder="What are you looking for?"><br><br>
            <input type="submit" value="submit">
          </form>
        </div>

        <div class="nav-top__employee">
          <a href="#">Employee Portal</a>
        </div>

        <div class="nav-top__membership">
          <a href="#">Membership</a>
          <div class="membership-dropdowns">
            <div class="link"><a href="#">Become a Member</a></div>
            <div class="link"><a href="#">Log In</a></div>
          </div>
        </div>
      </div>


      <div class="nav-bottom">
         <!-- nav menu -->
         <nav class="nav-main" onClick="">
          <?php
            if (has_nav_menu('primary_navigation')) :
              wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav nav-pills'));
            endif;
          ?>
        </nav>
      </div>
    </div>

  </div>








  <div class="nav-mobile">
    <div class="nav-top">
      <div class="nav-top__employee">
        <a href="#">Employee Portal</a>
      </div>

      <div class="nav-top__membership">
        <a href="#">Membership</a>
        <div class="membership-dropdowns">
            <div class="link"><a href="#">Become a Member</a></div>
            <div class="link"><a href="#">Log In</a></div>
        </div>
      </div>
    </div>


    <div class="nav-middle">
      <a class="brand" href="<?php echo home_url('/') ?>">
        <img src="http://sbci-new.ctclients.ca/wp-content/uploads/2020/10/image-logo.png">
      </a>
      <svg id="search-icon" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 21 27.75"><defs><style>.cls-1,.cls-3{fill:none;}.cls-2{clip-path:url(#clip-path);}.cls-3{stroke:#042c33;stroke-miterlimit:10;stroke-width:3px;}</style><clipPath id="clip-path"><rect class="cls-1" width="21" height="27.75"/></clipPath></defs><title>search-icon</title><g class="cls-2"><circle class="cls-3" cx="10.5" cy="10.5" r="9"/><line class="cls-3" x1="10.75" y1="19.75" x2="10.75" y2="27.75"/></g></svg>
        <div class="search_expanded">
          <form action="/action_page.php">
            <input type="text" id="search" name="search" placeholder="What are you looking for?"><br><br>
            <input type="submit" value="submit">
          </form>
        </div>
    
    </div>


    <div class="nav-bottom">
      <nav class="nav-main" onClick="">
        <?php
          if (has_nav_menu('primary_navigation')) :
            wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav nav-pills'));
          endif;
        ?>
      </nav>
      <!-- <div class="white-fade"></div> -->
      <div class="scroll-arrow">
        <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 11.06 17.89"><defs><style>.cls-1,.cls-3{fill:none;}.cls-2{clip-path:url(#clip-path);}.cls-3{stroke:#042c33;stroke-miterlimit:10;stroke-width:3px;}</style><clipPath id="clip-path" transform="translate(0 0)"><rect class="cls-1" width="11.06" height="17.89"/></clipPath></defs><title>left-arrow</title><g class="cls-2"><polyline class="cls-3" points="10 16.83 2.12 8.94 10 1.06"/></g></svg>
      </div>
    </div>
  </div>



  <div class="modal__container">
      
      <div class="modal hide">
      <a href="#" class="modal__close"><?php get_template_part('templates/icon__plus.svg'); ?></a>
          
          <div class="modal-block__left">

            <div class="title-container">
              <div class="icon"><?php get_template_part('templates/icon__membership.svg'); ?></div>
              <p class="title_left font__secondary--28"><?php _e('Membership Login', 'sbci'); ?></p>
            </div>
            
            <div class="login-form">
              <?php echo do_shortcode('[gravityform id="3" title="false" description="false"]'); ?>
            </div>

            <p class="forgot_link"><a href="#"><?php _e('Forgot your information?', 'sbci'); ?></a></p>

          </div>

          <div class="modal-block__right" style="background-image: url('/wp-content/themes/sbci/assets/img/image--texture.png')">

            <div class="container-right">
              <p class="title_right font__primary--26"><?php _e("Don't have an account?", 'sbci'); ?></p>
              <div class="member-link btn__secondary"><a href="#"><?php _e('Become a Member', 'sbci'); ?></a></div>
            </div>

          </div>


    </div><!-- end modal modal--border hide -->
</div><!-- end modal__container -->


</header>
