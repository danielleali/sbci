<?php
/**
 * Contact Us Module
 * Variables: $section_count, $type, $contact_title, $contact_description, $contact_location, 
 * $contact_phone, $contact_phone_toll_free, $contact_fax_toll_free, $contact_email, $admin_title, $administrators
 */
    extract($args);
    $num_of_locations = count($contact_location);
    $num_of_administrators = count($administrators);
?>

<div class="page_section section__contact" data-gallery="<?php echo $section_count; ?>" id="section__<?php echo $section_count; ?>">
    <div class="wrapper inner">

        <h3 class="font__primary--40 maintitle"><?php _e($section_title,'cinnamontoast');?></h3>

        <div class="container">

            <div class="container_left">

                <?php if ($contact_title) { ?>
                    <p class="title"><?php echo $contact_title; ?></p>
                <?php } ?>

                <?php if ($contact_description) { ?>
                    <p class="desc"><?php echo $contact_description; ?></p>
                <?php } ?>

                <?php if ($contact_location) { ?>
                    <div class="location-container">
                        <?php get_template_part('templates/icon__location.svg'); ?>
                        <div class="location-items">
                            <?php for($i = 0; $i <= $num_of_locations; $i++) { ?>
                                <p class="location"><?php print_r($contact_location[$i]['line']); ?></p>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>

                <div class="phone-container">
                    <?php get_template_part('templates/icon__phone.svg'); ?>
                    <?php if ($contact_phone) { ?>
                        <p class="phone"><?php echo $contact_phone; ?></p>
                    <?php } ?>
                    <?php if ($contact_phone_toll_free) { ?>
                        <p class="phone"><?php echo $contact_phone_toll_free; ?></p>
                    <?php } ?>
                </div>

                <div class="fax-container">
                    <?php get_template_part('templates/icon__fax.svg'); ?>
                    <?php if ($contact_fax) { ?>
                        <p class="fax"><?php echo $contact_fax; ?></p>
                    <?php } ?>
                    <?php if ($contact_fax_toll_free) { ?>
                        <p class="fax"><?php echo $contact_fax_toll_free; ?></p>
                    <?php } ?>
                </div>

                <?php if ($contact_email) { ?>
                    <p class="email btn__tertiary"><a href="mailto:<?php echo $contact_email; ?>"><?php echo $contact_email; ?></a></p>
                <?php } ?>

            </div>

            <div class="container_right">                    

                    <?php for($i = 0; $i < $num_of_administrators; $i++) { ?>

                        <?php
                            $person_id = $administrators[$i]->ID;
                            $a_title = get_field('a_title', $person_id);
                            $a_email = get_field('a_email', $person_id);
                            $a_phone = get_field('a_phone', $person_id);
                        ?>

                        <div class="content-container">

                            <div class="name-title-container">
                                <p class="name"><?php print_r($administrators[$i]->post_title); ?></p>
                                <p class="title">, <?php echo $a_title; ?></p>
                            </div>

                            <div class="contact-container">
                                <p class="email"><?php get_template_part('templates/icon__email.svg'); ?><?php echo $a_email; ?></p>
                                <p class="phone"><?php get_template_part('templates/icon__phone.svg'); ?><?php echo $a_phone; ?></p>
                            </div>

                        </div>
                        
                    <?php } ?>


            </div>

        </div>

    </div>
</div>