<?php
/**
 * Table module
 * Variables: $table
 */
    extract($args);
?>

<div class="page_section section__table" data-gallery="<?php echo $section_count; ?>" id="section__<?php echo $section_count; ?>">
    <div class="wrapper inner">
        <?php print_r( $table ); ?>
    </div>
</div>