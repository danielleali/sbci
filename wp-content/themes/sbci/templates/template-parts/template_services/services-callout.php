<?php
/**
 * Services Callout module
 * Variables: $section_count, $background_image, $title, $callout_title, $service_benefits
 */
    extract($args);

    if ($background_image) :
        $bg_img = $background_image['url'];
    else :
        $bg_img = '/wp-content/themes/sbci/assets/img/image--service-callout.jpg';
    endif;

?>

<div class="page_section section__services-callout" id="section__<?php echo $section_count; ?>" style="background-image: url('<?php echo $bg_img; ?>');">
    <div class="wrapper wide">

        <div class="container">

            <div class="block__left">
                <p class="title font__primary--40"><?php echo $title; ?></p>
            </div>

            <div class="block__right">
                <p class="font__primary--24"><?php echo $callout_title; ?></p>

                <?php foreach ($service_benefits as $benefit) { ?>

                    <?php 
                    if ( ( $benefit['checkmark_color'] == 'teal-light') ) :
                        $check_color = 'teal_light'; 
                    elseif ( ( $benefit['checkmark_color'] == 'teal-dark') ) :
                        $check_color = 'teal_dark';
                    elseif ( ( $benefit['checkmark_color'] == 'green-light') ) :
                        $check_color = 'green_light'; 
                    elseif ( ( $benefit['checkmark_color'] == 'green-dark') ) :
                        $check_color = 'green_dark'; 
                    endif;
                    ?>
                    <div class="item-icon__container">
                        <div class="circle <?php echo $check_color; ?>"><?php get_template_part('templates/icon__checkmark.svg'); ?></div>
                        <p class="benefit_item font__secondary--18"><?php print_r($benefit['benefit']); ?></p>
                    </div>
                <?php } ?>
            </div>

        </div>




        


    </div>
</div>
