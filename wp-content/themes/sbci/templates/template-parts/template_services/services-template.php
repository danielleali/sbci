<?php
/**
 * Services Offerings module
 * Variables: $section_count, links_title, $links, $main_content, $sub_content, $title, $accordions
 */
    extract($args);

?>

<div class="page_section section__services-template" id="section__<?php echo $section_count; ?>">
    <div class="wrapper">

        <div class="container">

            <div class="block__left">

                <p class="links_title font__primary--24"><?php echo $links_title; ?></p>

                <?php foreach ($links as $link_item) { ?>
                    <?php foreach ($link_item as $item) { ?>
                        <p class="links font__secondary--18">
                            <a href="<?php print_r($item['url']); ?>" target="<?php print_r($item['target']); ?>"><?php print_r($item['title']); ?></a>
                        </p>
                    <?php } ?>
                <?php } ?>

            </div>

            <div class="block__right">

                <div class="content_container">
                    <p class="main-content font__primary--26"><?php echo $main_content; ?></p>
                    <p class="sub-content font__secondary--18"><?php echo $sub_content; ?></p>
                    <div class=""></div>
                    <h2 class="title font__primary--40"><?php echo $title; ?></h2>
                </div>

                <div class="section__accordion">
                    <?php foreach ($accordions as $accordion_item) { ?>
                        <ul class="accordion active" data-cat="0">
                            <li>
                                <div class="question">
                                    <span class="btn "></span>
                                    <h4 class="title font__primary--24"><?php print_r( $accordion_item['accordion']->post_title ); ?></h4>
                                    <p class="label"><?php _e('Learn More', 'sbci'); ?></p>
                                </div>
                                <div class="answer">
                                    <div class="answer__content">
                                        <p class="font__secondary--18"><?php print_r( $accordion_item['accordion']->post_content ); ?></p>
                                    </div>
                                </div>

                            </li>
                        </ul>
                    <?php } ?>
                </div>

            </div>

        </div>

    </div>
</div>
