<?php
/**
 * Services Callout module
 * Variables: $section_count, $title, $content, $get_started_link, $member_box_title, $member_box_content
 */
    extract($args);

?>

<div class="page_section section__services-member" id="section__<?php echo $section_count; ?>">
    <div class="wrapper wide">

        <div class="container">
            <div class="block__left">
                <h2 class="title font__primary--40"><?php echo $title; ?></h2> 
            </div>
            <div class="block__middle">
                <p class="content font__primary--26"><?php echo $content; ?></p>
                <div class="btn__primary btn"><a href="<?php print_r($get_started_link['url']); ?>" target="<?php print_r($get_started_link['target']); ?>"><?php print_r($get_started_link['title']); ?></a></div>
            </div>
            <div class="block__right">
                <div class="icon"><?php get_template_part('templates/icon__membership.svg'); ?></div>
                <p class="member_title font__secondary--28"><?php echo $member_box_title; ?></p>
                <p class="member_content"><?php echo $member_box_content; ?></p>
            </div>
        </div>

    </div>
</div>
