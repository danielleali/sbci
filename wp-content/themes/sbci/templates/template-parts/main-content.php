<?php
/**
 * Main content module for text, images and half width quotes
 * Variables: $layout, $column_one_title, $column_two_title, $column_three_title, $column_one_content, $column_two_content, $column_three_content, 
 *            $column_one_image, $column_two_image, $image_one_caption, $image_two_caption, $column_one_quote, $column_two_quote
 */
    extract($args);
    // printf("section num:" . $section_count);

    // set our grid up
    if ($layout === 'three-col') {
        $grid_layout = 'grid grid--three-col';
    }
    if ($layout === 'two-col') {
        $grid_layout = 'grid grid--two-col';
    }
    if ($layout === 'one-col') {
        $grid_layout = 'grid grid--one-col';
    }


    if ($section_background_color === 'white') {
        $background = 'bg_white';
    } elseif ($section_background_color === 'grey') {
        $background = 'bg_grey';
    } 

?>

<div class="section_wrapper <?php esc_html_e($background); ?> <?php echo $section_count; ?>" >
    <div class="wrapper inner">

        <section class="section__main-content page_section <?php esc_html_e($grid_layout); ?>">

            <!-- COLUMN 1 -->
            <div class="column column--one animated fadeInDown">

                <!-- COL 1 = TEXT -->
                <?php if ($column_one_type === 'text') { ?>

                    <!-- Title -->
                    <?php if ($column_one_title !== ' ') : ?>
                        <?php  if ($layout === 'one-col') { // if one column, give different heading ?> 
                            <h3 class="font__secondary--28 title_one-col"><?php _e($column_one_title,'cinnamontoast');?></h3>
                        <?php  } else { ?>
                            <h3 class="font__primary--24 title"><?php _e($column_one_title,'cinnamontoast');?></h3>
                        <?php  } ?>
                    <?php endif; ?>

                    <!-- Content -->
                    <?php if ($column_one_content !== ' ') : // check if content is empty ?>
                       <p class="font__secondary--18 content"><?php _e($column_one_content,'cinnamontoast');?></p>
                    <?php endif; ?>

                <?php } ?>

                 <!-- COL 1 = IMAGE -->
                <?php if ($column_one_type === 'image') { ?>
                        <!-- //  IMAGE -->
                        <?php if ($column_one_image !== ' ') : ?>
                            <!-- // Image -->
                        <div class="image__holder">
                        <?php echo wp_get_attachment_image( $column_one_image, 'full', false, array('class' => 'image')); ?>
                        
                        <?php if  ($image_one_caption !== '') : ?>
                            <?php if ($layout === 'one-col') { ?>
                                <p class="image__caption one_col"><?php _e($image_one_caption,'cinnamontoast');?></p>
                            <?php } else { ?>
                                <p class="image__caption"><?php _e($image_one_caption,'cinnamontoast');?></p>
                           <?php } ?>
                        <?php endif; ?>
                        </div>
                    <?php endif; ?>
                <?php } ?>

                 <!-- COL 1 = QUOTE -->
                <?php if ($column_one_type === 'quote') { 
                    if ($column_one_quote !== '') :
                        $quote_arrays = $column_one_quote['pull_quote'];
                        $pq_count = 0;
                    ?>

                    <div class="pull_quote_new full">

                        <div class="pq_icon">
                            <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 72.24 52.32"><title>quotes-black</title><path d="M-24.52,199.77c-9,0-15.17-5.61-16.18-14.86a35.76,35.76,0,0,1,3.86-21.26c4.6-8.6,11.54-14.16,21.31-15.91,1-.18,1.51,0,1.62,1.09.15,1.43.34,2.86.54,4.28.13.9-.34,1.15-1.12,1.33a24.68,24.68,0,0,0-9.59,4.22,6.16,6.16,0,0,0-2.46,6.13,2.12,2.12,0,0,0,1.19,1.69,15.65,15.65,0,0,0,2.89,1.14A20,20,0,0,1-12.21,174c4.61,5.67,4.41,14.51-.29,20.48C-15.59,198.37-19.72,199.79-24.52,199.77Z" transform="translate(81.1 -147.68)"/><path d="M-81.09,180.68c0-10,3.46-18.69,10.79-25.66a27.8,27.8,0,0,1,14.82-7.11c.83-.14,1.28.07,1.35,1,.12,1.43.27,2.86.52,4.28.18,1-.24,1.27-1.07,1.44a25.58,25.58,0,0,0-8.41,3.4c-2.28,1.44-3.9,3.3-3.77,6.23A2.77,2.77,0,0,0-64.94,167c1.61.59,3.27,1.08,4.86,1.72,3.77,1.52,7.06,3.79,9,7.41,3.3,6.18,2.6,12.26-1.28,18-2.56,3.8-6.36,5.49-10.8,5.8-5.24.36-9.91-1-13.62-4.93-2.48-2.61-3.51-5.85-4-9.31A29.1,29.1,0,0,1-81.09,180.68Z" transform="translate(81.1 -147.68)"/></svg>
                        </div>

                        <div class="pull_quote_new_slider">
                            <ul>
                                <?php
                                $i=0;
                                foreach ($quote_arrays as $quote_array) { ?>
                                    
                                    <li <?php if($i==0): ?>class="active" <?php endif; ?>data-slide="<?php echo $i; ?>">
                                        <p class="quote">
                                            <?php print_r( $quote_array['quote'] ); ?>
                                        </p>
                                        <div class="quote_bottom">
                                            <p class="author">
                                    <?php print_r( $quote_array['author'] ); ?><?php if ( $quote_array['author_title'] ):?>,<?php endif; ?>
                                                <span class="author_title">
                                                    <?php print_r( $quote_array['author_title'] ); ?>
                                                </span>
                                            </p>
                                        </div>
                                    </li>
                                    
                                <?php $i++;
                                } ?>
                            </ul>
                        </div>

                        <div class="pq_slider__toggle autoslide">
                            <ul class="toggle_list">
                                <?php $i=0;
                                foreach ($quote_arrays as $quote_array) { ?>
                                    <li <?php if($i==0): ?>class="active" <?php endif; ?>data-slide="<?php echo $i; ?>"></li>
                                    <?php $i++;
                                } ?>
                            </ul>
                        </div>
                    </div>
                        
                    <?php endif; ?>
                <?php } ?>


                <!-- COL 1 = VIDEO -->
                <?php if ($column_one_type === 'video' && $column_two_type === 'video') { ?>
                    <?php if ($section_title_videos != '') { ?>

                        <?php  if ($layout === 'two-col') { ?>
                            <?php if ($column_two_type === 'video') { ?>
                                <div class="video_section_title stretch"><?php echo $section_title_videos; ?></div>
                            <?php } ?>
                        <?php } else { ?>
                            <div class="video_section_title"><?php echo $section_title_videos; ?></div>
                        <?php } ?>

                    <?php } ?>
                <?php } ?>


                <?php if ($column_one_type === 'video') { 
                    if ($column_one_video !== '') : ?>


                        

                        <?php  if ($layout === 'one-col') { ?>
                            <?php  if ($add_content_beside_video == 1) { ?>
                                <div class="video-container with-content">
                            <?php  } else { ?>
                                <div class="video-container full">
                            <?php  } ?>
                            
                        <?php  } else { ?>
                            <div class="video-container half">
                        <?php  } ?>

                        

                            <div class="video__wrapper">

                                <div class="overlay" style="background-image: url('<?php echo $column_one_video_overlay['url']; ?>')">
                                    <?php if( get_sub_field('video_title')){ ?>
                                        <h3 class="title heading__primary--24">VIDEO TITLE</h3>
                                    <?php } ?>
                                </div>

                                <div class="color-overlay">
                                    <button class="video__play btn__primary">
                                        <?php _e('Play','cinnamontoast');?>
                                    </button>
                                </div>

                                <?php
                                // Load value.
                                $iframe = get_sub_field('video'); // column_one_video

                                // Use preg_match to find iframe src.
                                preg_match('/src="(.+?)"/', $column_one_video, $matches);
                                $src = $matches[1];

                                // Add extra parameters to src and replcae HTML.
                                $params = array(
                                    'controls'  => 1,
                                    'hd'        => 1,
                                    'autohide'  => 1,
                                    'rel'       => 0,
                                    'autoplay'  => 0,
                                    'modestbranding' => 1
                                );
                                $new_src = add_query_arg($params, $src);
                                $column_one_video = str_replace($src, $new_src, $column_one_video);

                                // Add extra attributes to iframe HTML.
                                $attributes = 'frameborder="0"';
                                $column_one_video_iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $column_one_video);

                                // Display customized HTML.
                                echo $column_one_video_iframe;
                                ?>

                            </div>

                            <?php  if ($add_content_beside_video == 1) { ?>
                                <div class="video__caption">
                                    <p class="video_title"><?php if ($column_one_video_title != '') : echo $column_one_video_title; endif;?></p>
                                </div>
                                <div class="video__aside">
                                    <p class="aside_title"><?php if ($accompanying_title != '') : echo $accompanying_title; endif;?></p>
                                    <p class="aside_description"><?php if ($accompanying_description != '') : echo $accompanying_description; endif;?></p>
                                </div>
                            <?php  } else { ?>
                                <div class="video__caption">
                                    <p class="video_title"><?php if ($column_one_video_title != '') : echo $column_one_video_title; endif;?></p>
                                    <p class="video_description"><?php if ($column_one_video_description != '') : echo $column_one_video_description; endif;?></p>
                                </div>
                            <?php  } ?>

                            

                        </div>

                    <?php endif;
                } ?>

            </div>
            <!-- End COLUMN 1 -->
            


            <!-- COLUMN 2 -->
            <?php if ($layout === 'two-col' || $layout === 'three-col') : ?>
                <div class="column column--two animated fadeInDown">

                    <?php 
                    // Column 2 = text
                    if ($column_two_type === 'text') {

                        // Title
                        if ($column_two_title !== ' ') : //  check if title is empty
                            echo '<h3 class="font__primary--24 title">' . (__( $column_two_title, 'cinnamontoast' )) . '</h3>';
                        endif;

                        // Content
                        if ($column_two_content !== ' ') : // check if content is empty
                            echo '<p class="font__secondary--18 content">' . (__( $column_two_content, 'cinnamontoast' )) . '</p>';
                        endif;

                    }

                    // Column 2 = image
                    if ($column_two_type === 'image') {
                        if ($column_two_image !== ' ') :
                                // Image
                            echo '<div class="image__holder">';
                            echo wp_get_attachment_image( $column_two_image, 'full', false, array('class' => 'image'));
                            if  ($image_two_caption !== '') :
                                echo '<p class="image__caption">' . __($image_two_caption, 'cinnamontoast') . '</p>';
                            endif;
                            echo '</div>';
                        endif;
                    }

                    // Column 2 = quote
                    if ($column_two_type === 'quote') {
                        if ($column_two_quote !== '') :

                            $quote_arrays = $column_two_quote['pull_quote'];

                            ?>

                            <div class="pull_quote_new half">

                            <div class="pq_icon">
                                <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 72.24 52.32"><title>quotes-black</title><path d="M-24.52,199.77c-9,0-15.17-5.61-16.18-14.86a35.76,35.76,0,0,1,3.86-21.26c4.6-8.6,11.54-14.16,21.31-15.91,1-.18,1.51,0,1.62,1.09.15,1.43.34,2.86.54,4.28.13.9-.34,1.15-1.12,1.33a24.68,24.68,0,0,0-9.59,4.22,6.16,6.16,0,0,0-2.46,6.13,2.12,2.12,0,0,0,1.19,1.69,15.65,15.65,0,0,0,2.89,1.14A20,20,0,0,1-12.21,174c4.61,5.67,4.41,14.51-.29,20.48C-15.59,198.37-19.72,199.79-24.52,199.77Z" transform="translate(81.1 -147.68)"/><path d="M-81.09,180.68c0-10,3.46-18.69,10.79-25.66a27.8,27.8,0,0,1,14.82-7.11c.83-.14,1.28.07,1.35,1,.12,1.43.27,2.86.52,4.28.18,1-.24,1.27-1.07,1.44a25.58,25.58,0,0,0-8.41,3.4c-2.28,1.44-3.9,3.3-3.77,6.23A2.77,2.77,0,0,0-64.94,167c1.61.59,3.27,1.08,4.86,1.72,3.77,1.52,7.06,3.79,9,7.41,3.3,6.18,2.6,12.26-1.28,18-2.56,3.8-6.36,5.49-10.8,5.8-5.24.36-9.91-1-13.62-4.93-2.48-2.61-3.51-5.85-4-9.31A29.1,29.1,0,0,1-81.09,180.68Z" transform="translate(81.1 -147.68)"/></svg>
                            </div>

                            <div class="pull_quote_new_slider">
                                <ul>
                                    <?php
                                    $i=0;
                                    foreach ($quote_arrays as $quote_array) { ?>
                                        
                                        <li <?php if($i==0): ?>class="active" <?php endif; ?>data-slide="<?php echo $i; ?>">
                                            <p class="quote">
                                                <?php print_r( $quote_array['quote'] ); ?>
                                            </p>
                                            <div class="quote_bottom">
                                                <p class="author">
                                        <?php print_r( $quote_array['author'] ); ?><?php if ( $quote_array['author_title'] ):?>,<?php endif; ?>
                                                    <span class="author_title">
                                                        <?php print_r( $quote_array['author_title'] ); ?>
                                                    </span>
                                                </p>
                                            </div>
                                        </li>
                                        
                                    <?php $i++;
                                    }  
                                    ?>
                            

                                </ul>
                            </div>

                            <div class="pq_slider__toggle autoslide">
                                <ul class="toggle_list">
                                    <?php $i=0;
                                    foreach ($quote_arrays as $quote_array) { ?>
                                        <li <?php if($i==0): ?>class="active" <?php endif; ?>data-slide="<?php echo $i; ?>"></li>
                                        <?php $i++;
                                    } ?>
                                </ul>
                            </div>
                            </div>

                            
                            <?php 
                            
                        endif;
                    }
                    ?>

                    <!-- COL 2 = VIDEO -->

                    <?php if ($column_two_type === 'video') { 
                        if ($column_two_video !== '') : ?>


                            <!-- <div class="video_section_title hide">2 col< ?php echo $section_title_videos; ?></div> -->

                            <?php if ($column_one_video !== '') { ?>
                                <!-- <div class="video_section_title">video to the left</div> -->
                            <?php } ?>

                            <?php  if ($layout === 'one-col') { ?>
                                <div class="video-container full">
                            <?php  } else { ?>
                                <?php if ($section_title_videos !== '') { ?>
                                    <div class="video-container half title-included">
                                <?php } else { ?>
                                    <div class="video-container half">
                                <?php  } ?>
                            <?php  } ?>

                                <div class="video__wrapper">

                                    <div class="overlay" style="background-image: url('<?php echo $column_two_video_overlay['url']; ?>')">

                                        
                                    </div>
                                    <div class="color-overlay">
                                        <button class="video__play btn__primary">
                                            <?php _e('Play','cinnamontoast');?>
                                        </button>
                                    </div>

                                    <?php
                                    // Load value.
                                    $iframe = get_sub_field('video'); // column_one_video

                                    // Use preg_match to find iframe src.
                                    preg_match('/src="(.+?)"/', $column_two_video, $matches);
                                    $src = $matches[1];

                                    // Add extra parameters to src and replcae HTML.
                                    $params = array(
                                        'controls'  => 1,
                                        'hd'        => 1,
                                        'autohide'  => 1,
                                        'rel'       => 0,
                                        'autoplay'  => 0,
                                        'modestbranding' => 1
                                    );
                                    $new_src = add_query_arg($params, $src);
                                    $column_two_video = str_replace($src, $new_src, $column_two_video);

                                    // Add extra attributes to iframe HTML.
                                    $attributes = 'frameborder="0"';
                                    $column_two_video_iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $column_two_video);

                                    // Display customized HTML.
                                    echo $column_two_video_iframe;
                                    ?>

                                </div>

                                <div class="video__caption">
                                    <p class="video_title"><?php if ($column_two_video_title != '') : echo $column_two_video_title; endif;?></p>
                                    <p class="video_description"><?php if ($column_two_video_description != '') : echo $column_two_video_description; endif;?></p>
                                </div>

                            </div>

                        <?php endif;
                    } ?>

                </div>
            <?php endif; ?> 
            <!-- End COLUMN 2 -->






            <!-- COLUMN 3 -->
            <?php if ($layout === 'three-col' ) : ?>
                <div class="column column--three animated fadeInDown">

                    <?php

                        // Column 3 = text
                        if ($column_three_type === 'text') {
               
                            // Title
                            if ($column_three_title !== ' ') : //  check if title is empty
                                echo '<h3 class="font__primary--24 title">' . (__( $column_three_title, 'cinnamontoast' )) . '</h3>';
                            endif;

                            // Content
                            if ($column_three_type === 'text') {
                                if ($column_three_content !== ' ') : // check if content is empty
                                    echo '<p class="font__secondary--18 content">' . (__( $column_three_content, 'cinnamontoast' )) . '</p>';
                                endif;
                            }
                        }

                        // Column 3 = image
                        if ($column_three_type === 'image') {
                            // Display Image content (Half Width)
                            if ($column_three_image !== '') :
                                echo '<div class="image__holder">';
                                    echo wp_get_attachment_image( $column_three_image, 'full', false, array('class' => 'image'));
                                    if  ($image_three_caption !== '') :
                                        echo '<p class="image__caption">' . __($image_three_caption, 'cinnamontoast') . '</p>';
                                    endif;
                                echo '</div>';
                            endif;
                        }
                    ?>

                </div>
            <?php endif; ?> 
            <!-- End COLUMN 3 -->

        </section>
    </div>
</div>