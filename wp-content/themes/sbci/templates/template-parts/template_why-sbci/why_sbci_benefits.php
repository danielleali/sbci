<?php
/**
 * Why SBCI module
 * Variables: $title, $benefits
 */
    extract($args);

?>

<div class="page_section section__why-sbci-benefits" id="section__<?php echo $section_count; ?>" style="background-image: url('/wp-content/themes/sbci/assets/img/image--green-texture.jpg');">
    
    <div class="wrapper wrapper_top">

        <?php if ($title) { ?>
            <div class="section_title">
                <h3 class="font__primary--40 title"><?php echo $title; ?></h3>
            </div>
        <?php } ?>

        <div class="benefits_buckets">
            <?php $i = 1; ?>
            <?php foreach ($benefits as $benefit_item) { ?>
                <div class="bucket">
                    <div class="content-container">
                        
                        <div class="num-container">
                            <div class="card-num"><?php echo $i; ?></div>
                        </div>
                       
                        <?php if ($benefit_item['icon']) : ?>
                            <div class="icon"><img src="<?php print_r( $benefit_item['icon']['url'] ); ?>" alt="<?php print_r( $benefit_item['icon']['alt'] ); ?>"></div>
                        <?php else : ?>
                            <div class="icon"><?php get_template_part('templates/icon__benefits.svg'); ?></div>
                        <?php endif; ?>
                        
                        <p class="font__secondary--18 desc"><?php print_r( $benefit_item['description'] ); ?></p>
                    </div>
                </div>
                <?php $i++; ?>
            <?php } ?>
        </div>



    </div>

    <div class="wrapper wide wrapper_bottom"></div>
</div>
