<?php
/**
 * Why SBCI module
 * Variables: $content, $sub_content, $image_one, $image_two
 */
    extract($args);
    // printf("section num:" . $section_count);

?>
<div class="page_section section__why-sbci-callout" id="section__<?php echo $section_count; ?>" >
    <div class="wrapper">

        <div class="container">

            <div class="content-container">
                <div><p class="content font__primary--26"><?php echo $content; ?></p></div>
                <div><p class="subcontent font__secondary--18"><?php echo $sub_content; ?></p></div>
            </div>

            <div class="image-container">

                <div class="image_01-container">
                    <span class="leaf leaf-left leaf-med-01"><?php get_template_part('templates/icon__leaf-med-01.svg'); ?></span>
                    <span class="leaf leaf-left leaf-sm-01"><?php get_template_part('templates/icon__leaf-small-01.svg'); ?></span>
                    <img src="<?php echo $image_one['url']; ?>" alt="<?php echo $image_one['alt']; ?>">
                </div>

                <div class="image_02-container">
                    <img src="<?php echo $image_two['url']; ?>" alt="<?php echo $image_two['alt']; ?>">
                    <span class="leaf leaf-right leaf-sm-02"><?php get_template_part('templates/icon__leaf-small-02.svg'); ?></span>
                    <span class="leaf leaf-right leaf-big-01"><?php get_template_part('templates/icon__leaf-big-01.svg'); ?></span>
                    <span class="leaf leaf-right leaf-med-02"><?php get_template_part('templates/icon__leaf-med-02.svg'); ?></span>
                    <span class="leaf leaf-right leaf-sm-03"><?php get_template_part('templates/icon__leaf-small-03.svg'); ?></span>
                </div>

            </div>

        </div>

    </div>
</div>
