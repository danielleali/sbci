<?php
/**
 * Why SBCI module
 * Variables: $icon, $content, $link, $pull_quote

 */
    extract($args);

?>

<div class="page_section section__why-sbci-membership" id="section__<?php echo $section_count; ?>">
    <div class="wrapper">

        <div class="container">
            <div class="block__left">

                <?php if ($icon['icon']) : ?>
                    <div class="icon"><img src="<?php print_r( $icon['icon']['url'] ); ?>" alt="<?php print_r( $icon['icon']['alt'] ); ?>"></div>
                <?php else : ?>
                    <div class="icon"><?php get_template_part('templates/icon__membership.svg'); ?></div>
                <?php endif; ?>

                <?php if ($content) : ?>
                    <p class="font__primary--26"><?php echo $content; ?></p>
                <?php endif; ?>

                <?php if ($link) : ?>
                    <div class="btn__secondary"><a href="<?php print_r($link['url']); ?>" alt="<?php print_r($link['alt']); ?>" target=""><?php print_r($link['title']); ?></a></div>
                <?php endif; ?>

            </div>

            <div class="block__right">

                    <div class="pull_quote_new half">

                        <div class="pq_icon">
                            <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 72.24 52.32"><title>quotes-black</title><path d="M-24.52,199.77c-9,0-15.17-5.61-16.18-14.86a35.76,35.76,0,0,1,3.86-21.26c4.6-8.6,11.54-14.16,21.31-15.91,1-.18,1.51,0,1.62,1.09.15,1.43.34,2.86.54,4.28.13.9-.34,1.15-1.12,1.33a24.68,24.68,0,0,0-9.59,4.22,6.16,6.16,0,0,0-2.46,6.13,2.12,2.12,0,0,0,1.19,1.69,15.65,15.65,0,0,0,2.89,1.14A20,20,0,0,1-12.21,174c4.61,5.67,4.41,14.51-.29,20.48C-15.59,198.37-19.72,199.79-24.52,199.77Z" transform="translate(81.1 -147.68)"/><path d="M-81.09,180.68c0-10,3.46-18.69,10.79-25.66a27.8,27.8,0,0,1,14.82-7.11c.83-.14,1.28.07,1.35,1,.12,1.43.27,2.86.52,4.28.18,1-.24,1.27-1.07,1.44a25.58,25.58,0,0,0-8.41,3.4c-2.28,1.44-3.9,3.3-3.77,6.23A2.77,2.77,0,0,0-64.94,167c1.61.59,3.27,1.08,4.86,1.72,3.77,1.52,7.06,3.79,9,7.41,3.3,6.18,2.6,12.26-1.28,18-2.56,3.8-6.36,5.49-10.8,5.8-5.24.36-9.91-1-13.62-4.93-2.48-2.61-3.51-5.85-4-9.31A29.1,29.1,0,0,1-81.09,180.68Z" transform="translate(81.1 -147.68)"/></svg>
                        </div>

                        <div class="pull_quote_new_slider">
                            <ul>
                                <?php
                                $i=0;
                                foreach ($pull_quote as $quote_array) { ?>
                                    
                                    <li <?php if($i==0): ?>class="active" <?php endif; ?>data-slide="<?php echo $i; ?>">
                                        <p class="quote">
                                            <?php print_r( $quote_array['quote'] ); ?>
                                        </p>
                                        <div class="quote_bottom">
                                            <p class="author">
                                                <?php print_r( $quote_array['author'] ); ?><?php if ( $quote_array['author_title'] ):?>,<?php endif; ?>
                                                <span class="author_title">
                                                    <?php print_r( $quote_array['author_title'] ); ?>
                                                </span>
                                            </p>
                                        </div>
                                    </li>
                                    
                                <?php $i++;
                                } ?>
                            </ul>
                        </div>

                        <div class="pq_slider__toggle autoslide">
                            <ul class="toggle_list">
                                <?php $i=0;
                                foreach ($pull_quote as $quote_array) { ?>
                                    <li <?php if($i==0): ?>class="active" <?php endif; ?>data-slide="<?php echo $i; ?>"></li>
                                    <?php $i++;
                                } ?>
                            </ul>
                        </div>
                    </div>
                
            </div>
        </div>

    </div>
</div>
