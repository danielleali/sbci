<?php
/**
 * Logo Module for Partners
 * Variables: $section_count, $section_title, $section_background_color, $logos
 */
    extract($args);

    if ($section_background_color === 'white') {
        $background = 'bg_white';
    } elseif ($section_background_color === 'grey') {
        $background = 'bg_grey';
    }
?>



<div class="page_section section__logos <?php esc_html_e($background); ?>" data-gallery="<?php echo $section_count; ?>" id="section__<?php echo $section_count; ?>">
    <div class="wrapper inner">
        <h3 class="font__secondary--28 title"><?php _e($section_title,'cinnamontoast');?></h3>

        <div class="logos_container">
            <?php foreach( $logos as $image ): ?>
                <div class="logo"><?php echo wp_get_attachment_image($image, 'full', false, array('class' => 'image')); ?></div>
            <?php endforeach; ?>
        </div>  
    </div>
</div>
