<?php
/**
 * Accordion module for FAQ's
 * Variables: $section_count, $section_title, $accordions
 */
    extract($args);
?>

<div class="page_section section__accordion" data-gallery="<?php echo $section_count; ?>" id="section__<?php echo $section_count; ?>">
    <div class="wrapper inner">

        <?php if ($section_title) { ?>
            <div class="section_title">
                <h3 class="font__secondary--28 title"><?php echo $section_title; ?></h3>
            </div>
        <?php } ?>

        <?php foreach ($accordions as $accordion_item) { ?>
            <ul class="accordion active" data-cat="0">
                <li>

                    <div class="question">
                        <span class="btn "></span>
                        <h4 class="title font__primary--24"><?php print_r( $accordion_item['accordion']->post_title ); ?></h4>
                    </div>
                    
                    <div class="answer">
                        <div class="answer__content">
                            <p class="font__secondary--18"><?php print_r( $accordion_item['accordion']->post_content ); ?></p>
                        </div>
                    </div>

                </li>
            </ul>

            <!-- <div class="accordion">
                <h4 class="font__primary--24">< ?php print_r( $accordion_item['accordion']->post_title ); ?></h4>
                <p class="font__secondary--18">< ?php print_r( $accordion_item['accordion']->post_content ); ?></p>
            </div> -->
        <?php } ?>


    </div>
</div>
