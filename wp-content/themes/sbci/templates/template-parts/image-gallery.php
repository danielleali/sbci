<?php
/**
 * Main content module for text, images and half width quotes
 * Variables: $type, $gallery, $grid
 */
    extract($args);
    // printf("section num:" . $section_count);
?>






<!-- Photo Gallery -->
<?php if ($type === 'gallery') { ?>
    <div class="page_section section__img-gallery animated fadeInDown" data-gallery="<?php echo $section_count; ?>" id="section__<?php echo $section_count; ?>">
	<div class="wrapper inner">

        <?php if ($title) { ?>
            <div class="section_title">
                <h2><?php echo $title; ?></h2>
            </div>
        <?php } ?>


		<div class="container">
		<?php 
		$i = 0;
        if( $gallery ): 
            // printf("here:");
            // printf($gallery['url']);
            // printf($gallery['caption']);
            ?>
			<ul class="slider">
				<?php
                foreach( $gallery as $image ):
				?>
					<li class="<?php if($i == 0): echo 'active'; endif; ?> scrolling right">
                        <?php echo wp_get_attachment_image($image, 'full', false, array('class' => 'image')); ?>
                    </li>
				<?php
				$i++;
				endforeach;
				?>
			</ul>
		<?php endif; ?>
        </div>

        <div class="slider-container">
            <?php if($i > 0) : ?>
				<button class="btn btn__toggle btn__toggle-prev">
					<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 11.06 17.89"><defs><style>.cls-1,.cls-3{fill:none;}.cls-2{clip-path:url(#clip-path);}.cls-3{;stroke-miterlimit:10;stroke-width:3px;}</style><clipPath id="clip-path" transform="translate(0 0)"><rect class="cls-1" width="11.06" height="17.89"/></clipPath></defs><title>left-arrow</title><g class="cls-2"><polyline class="cls-3" points="10 16.83 2.12 8.94 10 1.06"/></g></svg>
				</button>
				<button class="btn btn__toggle btn__toggle-next">
                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 11.06 17.89"><defs><style>.cls-1,.cls-3{fill:none;}.cls-2{clip-path:url(#clip-path);}.cls-3{;stroke-miterlimit:10;stroke-width:3px;}</style><clipPath id="clip-path" transform="translate(0 0)"><rect class="cls-1" width="11.06" height="17.89"/></clipPath></defs><title>left-arrow</title><g class="cls-2"><polyline class="cls-3" points="10 16.83 2.12 8.94 10 1.06"/></g></svg>
				</button>
            <?php endif; ?>
            
            <ul class="slider__toggles">
            <?php foreach( $gallery as $image_id ): ?>
                <li class="">
                    <?php echo wp_get_attachment_image($image_id, 'full', false, array('class' => 'image')); ?>
                </li>
            <?php endforeach; ?>
            </ul>

        </div>
        
	</div>
</div>
<?php } ?>


<!-- Photo Grid -->
<?php if ($type === 'grid') { ?>
    <div class="page_section section__img-grid" data-gallery="<?php echo $section_count; ?>" id="section__<?php echo $section_count; ?>">
	    <div class="wrapper inner">

            <?php if ($title) { ?>
                <div class="section_title">
                    <h2><?php echo $title; ?></h2>
                </div>
            <?php } ?>

            <div class="masonry-with-columns">
                <?php foreach( $grid as $image ): ?>
                    
                    <div class="image-container animated fadeInDown">
                        <div class="image-overlay"><span class="line_horizontal"></span><span class="line_vertical"></span></div>
                        <?php echo wp_get_attachment_image($image, 'full', false, array('class' => 'image')); ?>
                    </div>
                <?php endforeach; ?>
            </div>     

	    </div>
    </div>
<?php } ?>