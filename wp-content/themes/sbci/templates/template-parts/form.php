<?php
/**
 * Form module
 * Variables: $form
 */
    extract($args);
?>

<div class="page_section section__form" data-gallery="<?php echo $section_count; ?>" id="section__<?php echo $section_count; ?>">
    <div class="wrapper inner">
        <div class="form__container">
            <?php gravity_form( $form , false, false, false, '', false ); ?>
        </div>
    </div>
</div>