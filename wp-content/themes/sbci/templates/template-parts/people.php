<?php
/**
 * Logo Module for Partners
 * Global Variables: $section_count, $type
 * School Board Variables: $sb_thumbnail, $school_board, $sb_location, $sb_title, $sb_email, $sb_phone, $sb_fax
 * Staff Variables: $s_award, $s_photo, $s_title, $s_email, $s_phone
 * Consultant Variables: $c_photo, $c_title, $c_email, $c_phone
 * 
 * $board_of_directors, $staff, $consultants,
 */
    extract($args);

?>

<div class="page_section section__people" data-gallery="<?php echo $section_count; ?>" id="section__<?php echo $section_count; ?>">
    <div class="wrapper">

        <h3 class="font__primary--40 maintitle"><?php _e($section_title,'cinnamontoast');?></h3>


        <!-- < ?php print_r( $board_of_directors[0]->ID ); ?> -->

        <!-- Number of Board of Directors: < ?php echo count($board_of_directors); ?><br>
        Number of Staff: < ?php echo count($staff); ?><br>
        Number of Consultants: < ?php echo count($consultants); ?><br> -->

        <?php
            // Count how many repeaters are used for each type
            $num_of_directors = count($board_of_directors);
            $num_of_staff = count($staff);
            $num_of_consultants = count($consultants);
        ?>

        <div class="people__container">

            <div class="people__side-menu">
                <p class="side_menu_title font__primary--24"><?php _e('Side Menu', 'cinnamontoast'); ?></p>
                <?php if ($board_of_directors) { ?>
                    <p class="side_menu_item active"><?php _e('Board of Directors', 'cinnamontoast'); ?></p>
                <?php } ?>
                <?php if ($staff) { ?>
                    <p class="side_menu_item"><?php _e('Staff', 'cinnamontoast'); ?></p>
                <?php } ?>
                <?php if ($consultants) { ?>
                    <p class="side_menu_item"><?php _e('Consultants', 'cinnamontoast'); ?></p>
                <?php } ?>
            </div>


            <div class="people__container-right">

                <div class="people__bod">
                    <h3 class="subtitle"><?php echo $bod_title; ?></h3>
                    <?php for($i = 0; $i < $num_of_directors; $i++) { ?>

                        <?php $sb_subtitle = $board_of_directors[$i]['sub_section_title_1'];?>
                        
                        <ul class="accordion active" data-cat="0">
                            <li>
                                <div class="question">
                                    <span class="btn "></span>
                                    <h4 class="title font__primary--24"><?php echo $sb_subtitle; ?></h4>
                                </div>
                        
                                <div class="answer">
                                    <div class="answer__content">

                                        <?php foreach( $board_of_directors[$i]['people'] as $person ) :
                                            $person_id = $person->ID;
                                            $sb_name = get_the_title($person_id);
                                            $sb_thumbnail = get_field('sb_thumbnail', $person_id);
                                            $school_board = get_field('school_board', $person_id);
                                            $sb_location = get_field('sb_location', $person_id);
                                            $sb_title = get_field('sb_title', $person_id);
                                            $sb_email = get_field('sb_email', $person_id);
                                            $sb_phone = get_field('sb_phone', $person_id);
                                            $sb_fax = get_field('sb_fax', $person_id); ?>

                                            <div class="people_content">

                                                <div class="container_top">
                                                    <?php if ($school_board) { ?>
                                                        <p class="school-board"><?php echo $school_board; ?></p>
                                                    <?php } ?>
                                                </div>

                                                <div class="container">
                                                    <div class="container_left">
                                                        <?php if ($sb_thumbnail) { ?>
                                                            <img src="<?php echo $sb_thumbnail; ?>">
                                                        <?php } ?>
                                                    </div>

                                                    <div class="container_right">

                                                        <div class="name-title-container">
                                                            <p class="person_name"><?php echo $sb_name; ?></p>
                                                            <?php if ($sb_title) { ?>
                                                                <p class="person_title">, <?php echo $sb_title; ?></p>
                                                            <?php } ?>
                                                        </div>

                                                        <div class="contact__container">
                                                            <?php if ($sb_location) { ?>
                                                                <div class="contact-item">
                                                                    <?php get_template_part('templates/icon__location.svg'); ?>
                                                                    <p><?php echo $sb_location; ?></p>
                                                                </div>
                                                            <?php } ?>

                                                            <?php if ($sb_email) { ?>
                                                                <div class="contact-item">
                                                                <?php get_template_part('templates/icon__email.svg'); ?>
                                                                <p><?php echo $sb_email; ?></p>
                                                                </div>
                                                            <?php } ?>

                                                            <?php if ($sb_phone) { ?>
                                                                <div class="contact-item">
                                                                <?php get_template_part('templates/icon__phone.svg'); ?>
                                                                <p><?php echo $sb_phone; ?></p>
                                                                </div>
                                                            <?php } ?>

                                                            <?php if ($sb_fax) { ?>
                                                                <div class="contact-item">
                                                                <?php get_template_part('templates/icon__fax.svg'); ?>
                                                                <p><?php echo $sb_fax; ?></p>
                                                                </div>
                                                            <?php } ?>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div> <!-- end people_content -->
                                            
                                        <?php endforeach; ?>
                                    
                                    </div>
                                </div>
                            </li>
                        </ul>
                    <?php } ?>
                </div> <!-- end board_of_directors section -->


                <div class="people__staff">
                    <h3 class="subtitle"><?php echo $staff_title; ?></h3>
                    <?php for($i = 0; $i < $num_of_staff; $i++) { ?>

                        <?php $s_subtitle = $staff[$i]['sub_section_title_2'];?>
                        
                        <ul class="accordion active" data-cat="0">
                            <li>
                                <div class="question">
                                    <span class="btn "></span>
                                    <h4 class="title font__primary--24"><?php echo $s_subtitle; ?></h4>
                                </div>
                        
                                <div class="answer">
                                    <div class="answer__content">

                                        <?php foreach( $staff[$i]['people'] as $person ) :
                                            $person_id = $person->ID;
                                            $s_name = get_the_title($person_id);
                                            $s_title = get_field('s_title', $person_id);
                                            $s_thumbnail = get_field('s_photo', $person_id);
                                            $s_award = get_field('s_award', $person_id);
                                            $s_email = get_field('s_email', $person_id);
                                            $s_phone = get_field('s_phone', $person_id);?>

                                            <div class="people_content">

                                                <div class="container">
                                                    <div class="container_left">
                                                        <?php if ($s_thumbnail) { ?>
                                                            <img src="<?php echo $s_thumbnail; ?>">
                                                        <?php } ?>
                                                    </div>

                                                    <div class="container_right">

                                                        <div class="name-title-container">
                                                            <p class="person_name"><?php echo $s_name; ?></p>
                                                            <?php if ($s_title) { ?>
                                                                <p class="person_title">, <?php echo $s_title; ?></p>
                                                            <?php } ?>
                                                        </div>

                                                        <div class="contact__container">
                                                            <?php if ($s_location) { ?>
                                                                <div class="contact-item">
                                                                    <?php get_template_part('templates/icon__location.svg'); ?>
                                                                    <p><?php echo $s_location; ?></p>
                                                                </div>
                                                            <?php } ?>

                                                            <?php if ($s_email) { ?>
                                                                <div class="contact-item">
                                                                <?php get_template_part('templates/icon__email.svg'); ?>
                                                                <p><?php echo $s_email; ?></p>
                                                                </div>
                                                            <?php } ?>

                                                            <?php if ($s_phone) { ?>
                                                                <div class="contact-item">
                                                                <?php get_template_part('templates/icon__phone.svg'); ?>
                                                                <p><?php echo $s_phone; ?></p>
                                                                </div>
                                                            <?php } ?>

                                                            <?php if ($s_fax) { ?>
                                                                <div class="contact-item">
                                                                <?php get_template_part('templates/icon__fax.svg'); ?>
                                                                <p><?php echo $s_fax; ?></p>
                                                                </div>
                                                            <?php } ?>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div> <!-- end people_content -->
                                            
                                        <?php endforeach; ?>
                                    
                                    </div>
                                </div>
                            </li>
                        </ul>
                    <?php } ?>
                </div> <!-- end staff section -->



                <div class="people__consultants">
                    <h3 class="subtitle"><?php echo $consultants_title; ?></h3>
                    <?php for($i = 0; $i < $num_of_consultants; $i++) { ?>

                        <?php $c_subtitle = $consultants[$i]['sub_section_title_3'];?>
                        
                        <ul class="accordion active" data-cat="0">
                            <li>
                                <div class="question">
                                    <span class="btn "></span>
                                    <h4 class="title font__primary--24"><?php echo $c_subtitle; ?></h4>
                                </div>
                        
                                <div class="answer">
                                    <div class="answer__content">

                                        <?php foreach( $consultants[$i]['people'] as $person ) :
                                            $person_id = $person->ID;
                                            $c_name = get_the_title($person_id);
                                            $c_title = get_field('c_title', $person_id);
                                            $c_thumbnail = get_field('c_photo', $person_id);
                                            $c_email = get_field('c_email', $person_id);
                                            $c_phone = get_field('c_phone', $person_id);
                                            ?>

                                            <div class="consultant_card"> <!-- card -->

                                                <?php if ($c_thumbnail) { ?>
                                                    <div class="container_top">
                                                        <img src="<?php echo $c_thumbnail; ?>">
                                                    </div>
                                                <?php } ?>

                                                <div class="container_bottom">

                                                    <?php if ($c_name) { ?>
                                                        <p class="c_name">
                                                            <?php echo $c_name; ?>
                                                        </p>
                                                    <?php } ?>

                                                    <?php if ($c_title) { ?>
                                                        <p class="c_title">
                                                            <?php echo $c_title; ?>
                                                        </p>
                                                    <?php } ?>

                                                    <div class="contact__container">

                                                        <?php if ($c_email) { ?>
                                                            <div class="contact-item">
                                                                <?php get_template_part('templates/icon__email.svg'); ?>
                                                                <p><?php echo $c_email; ?></p>
                                                            </div>
                                                        <?php } ?>

                                                        <?php if ($c_phone) { ?>
                                                            <div class="contact-item">
                                                                <?php get_template_part('templates/icon__phone.svg'); ?>
                                                                <p><?php echo $c_phone; ?></p>
                                                            </div>
                                                        <?php } ?>

                                                    </div>

                                                </div>
                                            </div> <!-- end people_content -->
                                            
                                        <?php endforeach; ?>
                                    
                                    </div>
                                </div>
                            </li>
                        </ul>
                    <?php } ?>
                </div> <!-- end staff section -->


            </div>
             


        </div>

        


        


        


        
    </div>
</div>

