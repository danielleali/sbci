
var projectSlug = 'ct-base';



var gulp = require('gulp'),
watch = require('gulp-watch');


require('es6-promise').polyfill();

gulp.task('watch', function() {
    gulp.watch(['assets/js/app.js'], ['scripts-dev']);
    gulp.watch(['assets/sass/app.scss', 'assets/sass/responsive.scss', 'assets/sass/print.scss', 'assets/sass/global/*.scss', 'assets/sass/section/*.scss'], ['sass-main-dev']);
    gulp.watch(['assets/sass/admin.scss', 'assets/sass/print.scss'], ['sass-various-dev']);
});

gulp.task('build', ['scripts', 'sass-main','sass-various']);


/* -----------------------------
 Dev Tasks without minification
 ------------------------------- */

 gulp.task('scripts-dev', function() {
    var jshint = require('gulp-jshint'),
    rename = require("gulp-rename");

    return gulp.src('assets/js/app.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(rename({
        basename:"main",
        extname: ".min.js"
    }))
    .pipe(gulp.dest('assets/js/'));
});

 gulp.task('sass-main-dev', function() {
    //var scsslint = require('gulp-scss-lint');
    var sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    rename = require("gulp-rename");

    return gulp.src(['assets/sass/app.scss'])
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(rename({
        basename:"main",
        extname: ".min.css"
    }))
    .pipe(gulp.dest('assets/css/'));
});

 gulp.task('sass-various-dev',function() {
    var sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    rename = require("gulp-rename");

    return gulp.src(['assets/sass/admin.scss','assets/sass/print.scss'])
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(rename({
        extname: ".min.css"
    }))
    .pipe(gulp.dest('assets/css/'));
});

/* -----------------------------
   Build Tasks with minifcation
   ------------------------------- */

   gulp.task('scripts', function() {
    var jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    rename = require("gulp-rename");

    return gulp.src('assets/js/app.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(rename({
        basename:"main",
        extname: ".min.js"
    }))
    .pipe(uglify())
    .pipe(gulp.dest('assets/js/'));
});

   gulp.task('sass-main', function() {
    var sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifyCss = require('gulp-cssnano'),
    rename = require("gulp-rename");

    return gulp.src(['assets/sass/app.scss'])
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(rename({
        basename:"main",
        extname: ".min.css"
    }))
    .pipe(minifyCss())
    .pipe(gulp.dest('assets/css/'));
});

   gulp.task('sass-various',function() {
    var sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifyCss = require('gulp-cssnano'),
    rename = require("gulp-rename");

    return gulp.src(['assets/sass/admin.scss','assets/sass/print.scss'])
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(rename({
        extname: ".min.css"
    }))
    .pipe(minifyCss())
    .pipe(gulp.dest('assets/css/'));
});
